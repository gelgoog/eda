#ifndef EDA_H
#define EDA_H

#include "datatype.h"
#include "AES/aes.h"
#include "CRC/crc.h"
#include "FEC/fec.h"

typedef struct
{
    uint8_t info;
    uint8_t key[16];
    uint8_t length[2];
    uint8_t data[700];
    uint8_t output[2];
} workspace_t;

size_t SFtoPHDRlen(size_t sf);

#define TASK_NO_ERR 0
#define TASK_ERR 1
#define DECODE_SUCCESS 0x7F
#define DECODE_FAIL 0x80

// This function simply calculates a new size for outgoing messages.
// It assumes that you know the size of the MACPayload,
// then adds a number of bytes to accomodate headers, checksums, and FEC.
size_t CalculateEncapsulatedSize(size_t oldsize, size_t PHDRlen);

// data starts with PHDR | empty CRC | MHDR | MACPayload | Empty MIC | Empty CRC
// Also needs key
// length assumed to be filled properly
// output can be empty
// info needed for sf
// uplink/downlink bit not checked
int EncodeMIC (void* MAC);

// This encodes payload CRC and PHDR_CRC in place
int EncodeCRC (void* PHY);

// It is assumed that there is enough space in buffer,
// as this doubles the length of the message.
int EncodeFEC (void* RadioPHY);

int DecodeFEC (void* RadioPHY);

// Unlike EncodeCRC, this only checks the PHDR_CRC
int DecodeCRC (void* PHY);

int DecodeMIC (void* MAC);

#endif // EDA_H
