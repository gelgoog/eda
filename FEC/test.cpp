#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "fec.h"

size_t CompareError(void* str1, void* str2, size_t len);

int main()
{
	unsigned char message[256];
	
	size_t i;	
	for (i = 0; i < 256; i ++)
	{
		message[i] = i;
	}
	
	printf("\nHamming (8, 4) test.\n");
	printf("Data should be recoverable when one bit or less has been corrupted");
	printf("per encoded byte.\n");

	printf("\nInput: (256 bytes data)");
	for (i = 0; i < 256; i++)
	{
		if (i % 16 == 0) {
			printf("\n");
		} else if (i % 2 == 0) {
			printf(" ");
		}
		printf("%x %x ", message[i]>>4, message[i]&0xf);
	}
	printf("\n\n");
	unsigned char code[512];
	FECEncode(code, message, 256);
	printf("\nOutput: (512 bytes data)");
	for (i = 0; i < 256; i++)
	{
		if (i % 16 == 0) {
			printf("\n");
		} else if (i % 2 == 0) {
			printf(" ");
		}
		printf("%02x%02x", code[2*i], code[2*i+1]);
	}
	printf("\n\n");

	time_t t;
	srand((unsigned) time(&t));
	//To be honest, I don't care too much how random this is.
	
	unsigned char onebit[512];
	for (i = 0; i < 512; i++)
	{
		onebit[i] = code[i] ^ (1 << (rand() % 8));
	}

	printf("\nOutput with a 1-bit random change per byte: (512 bytes data)");
	for (i = 0; i < 256; i++)
	{
		if (i % 16 == 0) {
			printf("\n");
		} else if (i % 2 == 0) {
			printf(" ");
		}
		printf("%02x%02x", onebit[2*i], onebit[2*i+1]);
	}
	printf("\n\n");

	bool proper_decode;
	unsigned char decoded[256]; 
	proper_decode = FECDecode(decoded, onebit, 512);
	if (!proper_decode) {
		printf("Was not successfully decoded.\n");
	} else {
		printf("Was successfully decoded.\n");
		printf("%d", CompareError(message, decoded, 256));
		printf(" half-bytes do not match.\n");
		printf("\nDecoded Output: (256 bytes data)");
		for (i = 0; i < 256; i++)
		{
			if (i % 16 == 0) {
				printf("\n");
			} else if (i % 2 == 0) {
				printf(" ");
			}
			printf("%x %x ", decoded[i]>>4, decoded[i]&0xf);
		}
		printf("\n\n");
	}

	unsigned char twobit[512];
	for (i = 0; i < 512; i++)
	{
		uint8_t a, b, c;
		a = rand() % 8;
		b = (a + (rand() % 7) + 1) % 8;
		// This adds [1, 7] to a then makes it wrap around
		// to stay within 0 and 7.
		// adding 0 or 8 will make a == b.
		// This will ensure a != b, and b can be any other valid value.
		c = (1 << a) ^ (1 << b);
		/*
		printf("Bitmask %03d: 0b ", i);
		uint8_t j;
		for (j = 0; j < 8; j++)
		{
			printf("%d", (c >> j) & 0x01);
		};
		printf(" (applied to 0x%02x)", code[i]);
		printf("\n");
		*/
		twobit[i] = code[i] ^ c;
	}
	printf("\n\n");

	printf("\nOutput with a 2-bit random change per byte: (512 bytes data)");
	for (i = 0; i < 256; i++)
	{
		if (i % 16 == 0) {
			printf("\n");
		} else if (i % 2 == 0) {
			printf(" ");
		}
		printf("%02x%02x", twobit[2*i], twobit[2*i+1]);
	}
	printf("\n\n");

	proper_decode = FECDecode(decoded, twobit, 512);
	if (!proper_decode) {
		printf("Was not successfully decoded.\n");
	} else {
		printf("Was successfully decoded.\n");
		printf("%d", CompareError(message, decoded, 256));
		printf(" half-bytes do not match.\n");
		printf("\nDecoded Output: (256 bytes data)");
		for (i = 0; i < 256; i++)
		{
			if (i % 16 == 0) {
				printf("\n");
			} else if (i % 2 == 0) {
				printf(" ");
			}
			printf("%x %x ", decoded[i]>>4, decoded[i]&0xf);
		}
		printf("\n\n");
	}

	return 0;
}

size_t CompareError(void* str1, void* str2, size_t len)
{
	size_t error = 0;
	unsigned char* s1 = (unsigned char*) str1;
	unsigned char* s2 = (unsigned char*) str2;

	size_t i;
	for(i = 0; i < len; i++)
	{
		if ((s1[i] & 0x0f) != (s2[i] & 0x0f)) {
			error++;
			//printf("Error: %x is not %x\n",s1[i] & 0x0f,s2[i] & 0x0f);
		}
		if ((s1[i] & 0xf0) != (s2[i] & 0xf0)) {
			error++;
			//printf("Error: %x is not %x\n",s1[i] & 0xf0,s2[i] & 0xf0);
		}
	}
	return error;
}

