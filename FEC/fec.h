#ifndef FEC_H
#define FEC_H

#include "../datatype.h"

extern const uint8_t Generator[16];
extern const uint8_t Parity[256];

void FECEncode(void* dest, void* src, size_t length);
bool FECDecode(void* dest, void* src, size_t length);

#endif // FEC_H
