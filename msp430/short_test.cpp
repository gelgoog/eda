#include "eda.h"
#include "msptest.h"

int main()
{
    MSP_INIT();   
	int error;
    
	workspace_t testspace;
	workspace_t* testspace_p = &testspace;
	ws_init(testspace_p);

	ENCODE_MIC_START();
    EncodeMIC(testspace_p);
	ENCODE_MIC_END();

	ENCODE_CRC_START();
    EncodeCRC(testspace_p);
	ENCODE_CRC_END();

	ENCODE_FEC_START();
    EncodeFEC(testspace_p);
	ENCODE_FEC_END();
	
	ws_utod(testspace_p);

	DECODE_FEC_START();
    error = DecodeFEC(testspace_p);
	DECODE_FEC_END();
	
	DECODE_CRC_START();
    error = DecodeCRC(testspace_p);
	DECODE_CRC_END();

	DECODE_MIC_START();
    error = DecodeMIC(testspace_p);
	DECODE_MIC_END();
    
    return 0;
}
