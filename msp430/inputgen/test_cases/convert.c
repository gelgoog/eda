#include<stdio.h>
#include<stdlib.h>
#define STRLEN 1444
#define uint8_t unsigned char

typedef struct
{
    uint8_t info;
    uint8_t key[16];
    uint8_t length[2];
    uint8_t data[700];
    uint8_t output[2];
} workspace_t;

unsigned char htoi(unsigned char h);

int main()
{
	FILE* inputf;
	int i;
	inputf = fopen("input_06.txt", "r");
	if (fopen == NULL)
	{
		printf("File error.\n");
		return 1;
	}
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	workspace_t testspace;
	unsigned char buf;

	testspace.key[0] = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.key[0] <<= 4;
	testspace.key[0] |= htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}


	testspace.info = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.info <<= 4;
	testspace.info |= htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	buf = fgetc(inputf);
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	if (buf != ' ')
	{
		printf("\nFormat error.\n");
		fclose(inputf);
		return 1;				
	}

	for (i = 1; i < 15; i+= 2)
	{
		testspace.key[i+1] = htoi(fgetc(inputf));
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		testspace.key[i+1] <<= 4;
		testspace.key[i+1] |= htoi(fgetc(inputf));
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		testspace.key[i] = htoi(fgetc(inputf));
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		testspace.key[i] <<= 4;
		testspace.key[i] |= htoi(fgetc(inputf));
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		buf = fgetc(inputf);
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		if (buf != ' ')
		{
			printf("\nFormat error.\n");
			fclose(inputf);
			return 1;				
		}
		 
	}

	testspace.length[0] = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.length[0] <<= 4;
	testspace.length[0] |= htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}

	testspace.key[15] = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.key[15] <<= 4;
	testspace.key[15] |= htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	
	buf = fgetc(inputf);
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	if (buf != ' ')
	{
		printf("\nFormat error.\n");
		fclose(inputf);
		return 1;				
	}

	testspace.data[0] = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.data[0] <<= 4;
	testspace.data[0] |= htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.length[1] = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.length[1] <<= 4;
	testspace.length[1] |= htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	buf = fgetc(inputf);
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	if (buf != ' ')
	{
		printf("\nFormat error.\n");
		fclose(inputf);
		return 1;				
	}
	
	for (i = 1; i < 699; i+=2)
	{
		testspace.data[i+1] = htoi(fgetc(inputf));
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		testspace.data[i+1] <<= 4;
		testspace.data[i+1] |= htoi(fgetc(inputf));
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		testspace.data[i] = htoi(fgetc(inputf));
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		testspace.data[i] <<= 4;
		testspace.data[i] |= htoi(fgetc(inputf));
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		buf = fgetc(inputf);
		if (feof(inputf))
		{
			printf("\nEOF reached.\n");
			fclose(inputf);
			return 1;
		}
		if (buf != ' ')
		{
			printf("\nFormat error.\n");
			fclose(inputf);
			return 1;				
		}
	}


	testspace.output[0] = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.output[0] <<= 4;
	testspace.output[0] |= htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}

	testspace.data[699] = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.data[699] <<= 4;
	testspace.data[699] |= htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	buf = fgetc(inputf);
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	if (buf != ' ')
	{
		printf("\nFormat error.\n");
		fclose(inputf);
		return 1;				
	}
	fgetc(inputf);
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	fgetc(inputf);
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.output[1] = htoi(fgetc(inputf));
	if (feof(inputf))
	{
		printf("\nEOF reached.\n");
		fclose(inputf);
		return 1;
	}
	testspace.output[1] <<= 4;
	testspace.output[1] |= htoi(fgetc(inputf));
	fclose(inputf);
	
	
	printf("Read success!\n\n");

	printf("uint8_t info = 0x%02x;\n", testspace.info);
	printf("uint8_t key[16] = {");
	for (i = 0; i < 15; i++)
	{
		printf("0x%02x, ", testspace.key[i]);
	}
	printf("0x%02x};\n", testspace.key[15]);
	printf("uint8_t length[2] = {0x%02x, 0x%02x};\n", testspace.length[0], testspace.length[1]);
	printf("uint8_t data[700] = {");
	for (i = 0; i < 699; i++)
	{
		printf("0x%02x, ", testspace.data[i]);
	}
	printf("0x%02x};\n", testspace.data[699]);
	printf("uint8_t output[2] = {0x%02x, 0x%02x};\n", testspace.output[0], testspace.output[1]);
	
	return 0;
}

unsigned char htoi(unsigned char h)
{
	switch(h)
	{
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;
		case 'A':
		case 'a':
			return 0xa;
		case 'B':
		case 'b':
			return 0xb;
		case 'C':
		case 'c':
			return 0xc;
		case 'D':
		case 'd':
			return 0xd;
		case 'E':
		case 'e':
			return 0xe;
		case 'F':
		case 'f':
			return 0xf;
		default:
			return 0;
	}
	return 0;
}
