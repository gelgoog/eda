#ifndef MSPTEST_H
#define MSPTEST_H


//#define OUTPUT_MSP
/* If this is defined, then it's meant to be for msp430.
 * If not, it's a test version for the VM.
 *
 */

#define TESTSPACE testspace

void ws_init(workspace_t* ws);
void ws_utod(workspace_t* ws);
void ws_dtou(workspace_t* ws);
#ifdef OUTPUT_MSP
#include <msp430.h>

#define MSP_INIT() WDTCTL = WDTPW + WDTHOLD; \
	P1DIR |= 0xff; \
    P1OUT = 0x00

//Watchdog Timer
//P1 dir as output
//Initialize P1 output

#define ENCODE_MIC_START() P1OUT = 0xe0
#define ENCODE_MIC_END() P1OUT = 0xe1
#define ENCODE_CRC_START() P1OUT = 0xec
#define ENCODE_CRC_END() P1OUT = 0xed
#define ENCODE_FEC_START() P1OUT = 0xef
#define ENCODE_FEC_END() P1OUT = 0xe7

#define DECODE_FEC_START() P1OUT = 0xdf
#define DECODE_FEC_END() P1OUT = 0xd7
#define DECODE_CRC_START() P1OUT = error? 0xee : 0xdc
#define DECODE_CRC_END() P1OUT = 0xdd
#define DECODE_MIC_START() P1OUT = error? 0xee : 0xd0
#define DECODE_MIC_END() P1OUT = error? 0xee : 0xdd

#else // not OUTPUT_MSP
#include "edatest.h"

#define MSP_INIT()

#define ENCODE_MIC_START() PrintWorkspace(TESTSPACE, 1)
#define ENCODE_MIC_END() printf("After EncodeMIC:\n"); \
	PrintWorkspace(TESTSPACE, 0)
#define ENCODE_CRC_START() printf("EncodeCRC...\n")
#define ENCODE_CRC_END() printf("After EncodeCRC:\n"); \
    PrintWorkspace(TESTSPACE, 0)
#define ENCODE_FEC_START() printf("EncodeFEC...\n")
#define ENCODE_FEC_END() PrintWorkspace(TESTSPACE, 0)

#define DECODE_FEC_START() PrintWorkspace(TESTSPACE, 1)
#define DECODE_FEC_END() printf("After DecodeFEC: %01d\n", error); \
    PrintWorkspace(TESTSPACE, 0)
#define DECODE_CRC_START() printf("DecodeCRC...\n")
#define DECODE_CRC_END() printf("After DecodeCRC: %01d\n", error); \
    PrintWorkspace(TESTSPACE, 0)
#define DECODE_MIC_START() printf("DecodeMIC...\n")
#define DECODE_MIC_END() printf("After DecodeMIC: %01d\n", error); \
    PrintWorkspace(TESTSPACE, 0)

#endif // OUTPUT_MSP

#endif // MSPTEST_H
