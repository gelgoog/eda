#include "eda.h"
#include "msptest.h"

void ws_utod(workspace_t* ws)
{
	size_t msglen;
	// Now to manipulate the data to try to "decode" it.
	msglen = (ws->output[1] << 8) & 0xff00;
	msglen |= ws->output[0] & 0xff;
	msglen -= 4;	// Ignore the 8,4'd pCRC.
    ws->length[1] = (msglen >> 8) & 0xff;
    ws->length[0] = msglen & 0xff;
    ws->output[1] = 0x00;
    ws->output[0] = 0x00;

	return;
}

void ws_dtou(workspace_t* ws)
{
	size_t msglen;
	// This is a similar method.
	msglen = (ws->output[1] << 8) & 0xff00;
	msglen |= ws->output[0] & 0xff;
	msglen += 2;	// Add space for the pCRC.
	ws->data[msglen-1] = 0x00;	// Zero out the space
	ws->data[msglen-2] = 0x00;	// for the pCRC.
    ws->length[1] = (msglen >> 8) & 0xff;
    ws->length[0] = msglen & 0xff;
    ws->output[1] = 0x00;
    ws->output[0] = 0x00;

	return;
}


void ws_init(workspace_t* ws)
{
    size_t i;
    
    
    uint8_t info = 0x8c;
    uint8_t key[16] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
    uint8_t length[2] = {0x10, 0x01};
    uint8_t data[700] = {0xb0, 0x9c, 0x9e, 0xcb, 0xf4, 0x05, 0x9f, 0xcc, 0xd4, 0xb0, 0x00, 0x00, 0x64, 0x33, 0xfb, 0x44, 0x66, 0x9f, 0x40, 0xed, 0xd4, 0x4a, 0x8b, 0xde, 0xe8, 0x72, 0x11, 0x6d, 0x2b, 0x51, 0x1b, 0x9e, 0xab, 0x91, 0x6d, 0xa8, 0xa4, 0x90, 0x8d, 0xda, 0xcd, 0x11, 0x7c, 0x9d, 0x05, 0x58, 0x55, 0x5b, 0x24, 0x01, 0x3a, 0xcf, 0x0a, 0xf0, 0x1e, 0xaf, 0x88, 0x09, 0x96, 0x0e, 0x58, 0xe0, 0x91, 0x6f, 0x17, 0x02, 0xf4, 0xd6, 0xd7, 0xf6, 0x12, 0x22, 0x3f, 0x8f, 0x1c, 0x26, 0x1d, 0x40, 0x78, 0x92, 0x3f, 0x3c, 0xb6, 0x75, 0xe0, 0xe9, 0x87, 0xb9, 0x0c, 0xe8, 0xc0, 0x3a, 0x3d, 0x18, 0x01, 0xb8, 0x25, 0x5d, 0xad, 0x4b, 0x34, 0x47, 0x88, 0xea, 0x4f, 0x7a, 0x5f, 0xac, 0x5d, 0x4f, 0x15, 0xcf, 0x1d, 0x51, 0x04, 0xf5, 0x1f, 0xce, 0xa3, 0x43, 0xdf, 0xd7, 0x9a, 0x09, 0xe5, 0x9f, 0xba, 0xb4, 0x4a, 0x20, 0xcd, 0x58, 0x5a, 0x9a, 0x04, 0x5c, 0xf0, 0x7f, 0x57, 0x9e, 0x66, 0xac, 0xab, 0xe5, 0x31, 0x40, 0x1f, 0x69, 0x52, 0xb6, 0x26, 0x25, 0x94, 0x9e, 0x99, 0x4a, 0x82, 0xb8, 0xc7, 0x5e, 0x52, 0x01, 0xf3, 0xce, 0xca, 0x42, 0x21, 0xdf, 0x13, 0x07, 0x81, 0x4e, 0x63, 0x09, 0xd1, 0xf5, 0xd8, 0x9e, 0x7e, 0xb4, 0x4e, 0xa0, 0x25, 0xe0, 0x21, 0xd4, 0x66, 0x02, 0x43, 0xc5, 0x18, 0xfd, 0xa7, 0x36, 0x8e, 0x02, 0x23, 0x37, 0x47, 0xab, 0x45, 0x15, 0x4b, 0x1e, 0x1a, 0x08, 0x7b, 0xcf, 0x96, 0x0b, 0x6f, 0x2f, 0x64, 0xef, 0xf6, 0x2f, 0x29, 0x11, 0x9e, 0x10, 0x91, 0xa9, 0x67, 0x06, 0x85, 0x45, 0x83, 0x16, 0x12, 0x86, 0xc7, 0xa0, 0x44, 0x7a, 0xe3, 0x9f, 0x2d, 0x1a, 0x11, 0xb6, 0x2d, 0xf9, 0x34, 0x28, 0x17, 0x07, 0xee, 0xb3, 0xd7, 0x3e, 0xa1, 0x89, 0x3a, 0x87, 0xf0, 0x55, 0x1f, 0xaf, 0xd5, 0xe3, 0xb2, 0x25, 0x3c, 0xa6, 0x55, 0xab, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    uint8_t output[2] = {0x00, 0x00};
    
    ws->info = info;
    for (i = 0; i < 16; i++)
    {
        ws->key[i] = key[i];
    }
    for (i = 0; i < 700; i++)
    {
        ws->data[i] = data[i];
    }
    ws->length[1] = length[1];
    ws->length[0] = length[0];
    ws->output[1] = output[1];
    ws->output[0] = output[0];
	
	return;
}
