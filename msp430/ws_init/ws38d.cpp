#include "eda.h"
#include "msptest.h"

void ws_utod(workspace_t* ws)
{
	size_t msglen;
	// Now to manipulate the data to try to "decode" it.
	msglen = (ws->output[1] << 8) & 0xff00;
	msglen |= ws->output[0] & 0xff;
	msglen -= 4;	// Ignore the 8,4'd pCRC.
    ws->length[1] = (msglen >> 8) & 0xff;
    ws->length[0] = msglen & 0xff;
    ws->output[1] = 0x00;
    ws->output[0] = 0x00;

	return;
}

void ws_dtou(workspace_t* ws)
{
	size_t msglen;
	// This is a similar method.
	msglen = (ws->output[1] << 8) & 0xff00;
	msglen |= ws->output[0] & 0xff;
	msglen += 2;	// Add space for the pCRC.
	ws->data[msglen-1] = 0x00;	// Zero out the space
	ws->data[msglen-2] = 0x00;	// for the pCRC.
    ws->length[1] = (msglen >> 8) & 0xff;
    ws->length[0] = msglen & 0xff;
    ws->output[1] = 0x00;
    ws->output[0] = 0x00;

	return;
}


void ws_init(workspace_t* ws)
{
    size_t i;
    
    
	uint8_t info = 0x88;
	uint8_t key[16] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
	uint8_t length[2] = {0x48, 0x00};
	uint8_t data[700] = {0x33, 0x87, 0xd2, 0x55, 0x33, 0xaa, 0xcc, 0xe1, 0xaa, 0xaa, 0xaa, 0xff, 0xd2, 0xcc, 0x1e, 0x87, 0xff, 0x4b, 0x33, 0x66, 0xaa, 0xff, 0x99, 0x78, 0x33, 0x66, 0x33, 0x33, 0x33, 0x2d, 0x66, 0xe1, 0xe1, 0x99, 0xaa, 0xd2, 0xd2, 0xd2, 0xff, 0x87, 0x78, 0x55, 0x4b, 0xcc, 0x4b, 0xe1, 0x1e, 0x4b, 0x4b, 0x78, 0x4b, 0x66, 0x00, 0x2d, 0x66, 0x2d, 0xcc, 0x2d, 0x4b, 0xe1, 0x66, 0xcc, 0x1e, 0x4b, 0x4b, 0x00, 0x78, 0x1e, 0xd2, 0xd2, 0x99, 0xcc, 0xcc, 0xb4, 0xe1, 0x99, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x01, 0x7b, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x68, 0x02, 0x7b, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x9c, 0xaf, 0xfc, 0x26, 0x24, 0x7f, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x34, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x40, 0xad, 0xbf, 0x26, 0x24, 0x7f, 0x00, 0x00, 0x20, 0x7e, 0xc0, 0x26, 0x24, 0x7f, 0x00, 0x00, 0xc2, 0xb7, 0xfc, 0x26, 0x24, 0x7f, 0x00, 0x00, 0x36, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x7e, 0xc0, 0x26, 0x24, 0x7f, 0x00, 0x00, 0xc0, 0x34, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x08, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x04, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x50, 0xb1, 0xfc, 0x26, 0x24, 0x7f, 0x00, 0x00, 0x26, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x03, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x03, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x2e, 0x4e, 0x3d, 0xf6, 0x00, 0x00, 0x00, 0x00, 0x38, 0xf5, 0xd8, 0x03, 0x00, 0x00, 0x00, 0x00, 0x2e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x20, 0x7e, 0xc0, 0x26, 0x24, 0x7f, 0x00, 0x00, 0x04, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0xd0, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0xc8, 0x39, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68, 0x33, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0xe0, 0x64, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x70, 0x33, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x10, 0x3a, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x98, 0x33, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x11, 0xbc, 0xfc, 0x26, 0x24, 0x7f, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x3a, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x88, 0x61, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x20, 0x67, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0xa0, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x64, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0xe0, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x88, 0x61, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0xd0, 0x32, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x90, 0x03, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2e, 0x4e, 0x3d, 0xf6, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x01, 0x33, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x50, 0x72, 0xc0, 0x26, 0x24, 0x7f, 0x00, 0x00, 0xc0, 0x34, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x34, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xa0, 0x33, 0x73, 0xc0, 0xff, 0x7f, 0x00, 0x00, 0xc0, 0x34, 0x1e, 0x27, 0x24, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xdd, 0x2d, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x2d, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x40, 0x00, 0x00, 0x00, 0x00};
	uint8_t output[2] = {0x00, 0x00};
    
    ws->info = info;
    for (i = 0; i < 16; i++)
    {
        ws->key[i] = key[i];
    }
    for (i = 0; i < 700; i++)
    {
        ws->data[i] = data[i];
    }
    ws->length[1] = length[1];
    ws->length[0] = length[0];
    ws->output[1] = output[1];
    ws->output[0] = output[0];
	
	return;
}
