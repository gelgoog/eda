#ifndef DATATYPE_H
#define DATATYPE_H

#define uint8_t unsigned char
#define int8_t signed char

// FOR MSP430 via msp430-gcc
// http://mspgcc.sourceforge.net/manual/x987.html
#define uint16_t unsigned int
#define int16_t int
#define uint32_t unsigned long
#define int32_t long

//Still not sure how big size_t should be
#ifndef size_t
#define size_t unsigned int
#endif //size_t

/*
// FOR LINUX VM
#define uint16_t unsigned short
#define int16_t short
#define uint32_t unsigned int
#define int32_t int
// note: for the c9 workspace (64-bit VM)
// a long and a long long are both 64 bits.
// Sooooo... yeah
#define uint64_t unsigned long
#define int64_t long

//Still not sure how big size_t should be
#ifndef size_t
#define size_t unsigned short
#endif // size_t
*/

#endif // DATATYPE_H
