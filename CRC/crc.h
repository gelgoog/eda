#ifndef CRC_H
#define CRC_H

#include "../datatype.h"

// This polynomial is for CRC-ANSI/CRC-IBM/CRC
#define CRC_POLYNOMIAL 0x8005
#define CRC_POLYNOMIAL_REVERSE 0xA001
#define CRC_POLYNOMIAL_REVERSE_RECIPROCAL 0xC002

extern const uint16_t crcTable[256];
extern const uint16_t crcTable_r[256];
extern const uint16_t crcTable_rr[256];

void print_bin_16(uint16_t num);

uint16_t getCRC(void* src, size_t length);
void generateCRCTable(uint16_t*);

#endif // CRC_H
