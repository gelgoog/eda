#include <stdio.h>
#include "crc.h"

int main()
{

    unsigned char Message[] = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.";
    size_t msglen = 195;
    uint16_t CRC = 0x8394;
	size_t i;
    // CRC calculated using
    // https://www.ghsi.de/CRC/index.php?Polynom=11000000000000101

	printf("\nCRC-16 test. Main function is getCRC.\n");
	printf("Test case generated with an online CRC calculator.\n");
	printf("(https://www.ghsi.de/CRC/index.php?Polynom=11000000000000101)\n");
	printf("CRC polynomial taken from standard CRC-16 (CRC-16-IBM/CRC-16-ANSI)\n");
	printf(" 16   15   2   0\n");
	printf("x  + x  + x + x \n");

	printf("\nInitial message (length 195 bytes):\n");
	for (i = 0; i < msglen; i++)
	{
		if (i % 32 == 0) {
			printf("\n");
		} else if (i % 4 == 0) {
			printf(" ");
		}
		printf("%02x", Message[i]);
	}
	printf("\n");
	


    uint16_t test_CRC = getCRC(Message, msglen);
    printf("\nCRC expected:   %04x\nCRC calculated: %04x\n\n", CRC, test_CRC);
    return 0;
}
