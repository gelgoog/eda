#ifndef __VSCHED_H__
#define __VSCHED_H__

//===================================================================================================
//										COMPILER FLAGS	

// Welcome to the scheduler bistro. Please select the dishes you'd like from below	

	// APPETIZERS: VARIOUS MODES OF OPERATION (OPTIONAL)

//#define DEBUG					// makes the scheduler printf what it's doing to stdout
//#define INTEGRATE				// for those that prefer the classic scheduler_i experience. guest cook: al
//#define SIM					// please enable this if you're running on linux and want the dummy functions
								// (compatible with blind mode)

	// DRINKS: HOW DATATYPES ARE DEFINED (CHOOSE ONLY ONE)

#define STDINT					// enable this by default
//#define DATATYPEH				// enable this if you're compiling from the msp folder (../msp430)
								// otherwise it does not compile from here

	// DESERT: HOW INPUTS ARE FED INTO THE MACHINE

#define BLIND					// no inputs. runs on pre-hardcoded commands
//#define FILEIO				// reads from a file and writes selected vf pairs to a log
//#define MSP					// only enable when you want to run on an msp with a way to pass interrupts

	// MAIN COURSE: WHAT ALGORITHM WOULD YOU FANCY TODAY?

//#define DYNAMIC_DEADLINE		// enabling this requires the inputs to provide a deadline each time.
								// use in conjunction with one of the next two options
//#define CYCLE_CONSERVING		// frequency switching is done on a per-task basis
#define RATE_MONOTONIC			// frequency switching is done on a per-task-set basis

//===================================================================================================
//										INCLUDES AND COMPILE MODES	
#ifdef STDINT	
	#include <stdint.h>
#endif

#ifdef DATATYPEH
    #include "../datatype.h"
#endif

#ifdef INTEGRATE
	#include "../edatest.h"
#endif

#ifdef SIM
	#include <stdlib.h>	
	#include <stdio.h>		
#endif

#ifdef MSP
	#include <msp430.h>
#endif

#ifdef DEBUG
	#ifndef SIM
		#include <stdio.h>
	#endif
	#define PRINTF(x) printf x
#else
	#define PRINTF(x) do {} while (0)
#endif


//===================================================================================================
//										DEFINES	


#define MAX_VF_PAIR_ENTRIES		10
#define MAX_TASK_ENTRIES		10
#define MAX_TASK_SET_ENTRIES	10

#define REG_VOLTAGE CR_VSETTING
#define REG_FREQUENCY CR_FSETTING


volatile uint16_t VF_PAIR_ENTRIES;
volatile uint8_t TASK_ENTRIES;
volatile uint8_t TASK_SET_ENTRIES;

#define TASK_SET_RECEIVE	0x01
#define TASK_SET_TRANSMIT	0x02
#define TASK_SET_SWITCH		0x03
#define SLEEP_MODE_VOLTAGE		FVRS_Table[VF_PAIR_ENTRIES-1].voltageCode
#define SLEEP_MODE_FREQUENCY	FVRS_Table[VF_PAIR_ENTRIES-1].frequencyCode
#define MAX_VOLTAGE			FVRS_Table[0].voltageCode
#define MAX_FREQUENCY		FVRS_Table[0].frequencyCode
#define VS_MARGIN 10
												

//===================================================================================================
//										GLOBAL FLAGS		

struct VS_FLAGS {
	volatile uint8_t SYSTEM_EXIT : 1;		// flag for when the sensor must shut down
	volatile uint8_t WAKE : 1;				// flag for waking the processor up
	volatile uint8_t TASK_SET_ONGOING : 1;	// flag for if a task set is currently ongoing
} VS_FLAGS;


volatile uint16_t CR_FSETTING;				// control register for selected frequency setting -> simulation only!
volatile uint16_t CR_VSETTING;				// control register for selected voltage setting -> simulation only!
volatile uint8_t CR_TASK_SET;					// control register for current task set
volatile uint16_t CR_PACKET_LENGTH;				// control register for packet length in bytes

volatile uint16_t CR_DEADLINE;
volatile uint8_t CR_TASK_INDEX;
volatile uint8_t CR_TASK_INDEX_END;				// when the task_counter equals this, end the task set;
volatile uint8_t CR_VF_INDEX;					// for selecting the vf pair



void* VS_TASK_ARG;

#ifdef SIM
uint16_t currTime;						// TEST variable for number of elapsed time units
uint16_t inputi_helper;					// test variable for packet length transfer
uint16_t inputd_helper;					// test variable for deadline
#endif

#ifdef FILEIO
FILE *fp;								// For automated inputs
FILE *lp;								// For automated VF logging
#endif

//===================================================================================================
//										FUNCTIONS												

int VS_Init();															// Initializes all the lookup tables 
int VS_Sleep();															// Sets Voltage and Frequency to minimum until woken up
int VS_SelectTask();													// Selects the next task
int VS_ChangeVF(uint16_t voltage,uint16_t frequency);					// Changes the VF Control registers one bit at a time
int VS_TaskSetInit();													// Prepares the scheduler to run a whole task set

#ifdef CYCLE_CONSERVING
int VS_Cycle_Conserving();												// Selects the next operating point based on the task
#endif
#ifdef RATE_MONOTONIC
int VS_Rate_Monotonic();
#endif

int VS_AddTask(int (*func)(void*), uint16_t C0, uint16_t C1, uint8_t TaskSet);
int VS_AddTaskSet(uint8_t taskSetID, uint16_t deadline);
int VS_AddVFPair(		uint16_t frequency, 
						uint16_t voltage, 
						uint16_t relativeFrequency, 
						uint16_t upVoltageSlew, 
						uint16_t downVoltageSlew);
int import_FVRS_Settings();

uint16_t VS_Multiply(uint16_t value,uint16_t relative);		
uint16_t VS_GetSlew(uint8_t source_index, uint8_t dest_index);

// These functions are PROCESSOR-SPECIFIC :)

int VS_StartTime();							// Resets and starts timer
int VS_GetTime();							// Gives the current time in Time Units
int VS_StopTime();							// Stops timer to save power

#ifdef SIM
void int_receive();
void int_transmit();
void int_exit();
void int_switch();
#endif 

//	TEST FUNCTIONS

int dummy_FECR(void*);
int dummy_CRCR(void*);
int dummy_MICR(void*);
int dummy_FECT(void*);
int dummy_CRCT(void*);
int dummy_MICT(void*);
int dummy_Default(int,char[]);

#ifdef INTEGRATE
int msg_init();
int switch_message(void*);
#endif

#ifdef BLIND
int VS_AddBlindCommand(int index, char c, uint16_t length, uint16_t deadline);
int VS_BlindInit();
#endif

//===================================================================================================
//										LOOKUP TABLES					

struct FVRS_Table {
		uint16_t downVoltageSlew;
		uint16_t upVoltageSlew;
		uint16_t relativeFrequency;
		uint16_t voltageCode;
		uint16_t frequencyCode;	
} FVRS_Table[MAX_VF_PAIR_ENTRIES];

struct Task_Pairs {
	int (*taskID) (void*);
	uint16_t C0;
	uint16_t C1;
	uint8_t taskSet;
} Task_Pairs[MAX_TASK_ENTRIES];

struct Task_Sets {
	uint16_t deadline;
	uint8_t taskSetID;
} Task_Sets[MAX_TASK_SET_ENTRIES];

#ifdef BLIND
struct blind_commands {
	char c;
	uint16_t length;
	uint16_t deadline;
} blind_commands[10];
#endif



#endif
