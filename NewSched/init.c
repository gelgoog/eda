#include "vsched.h"


//===================================================================================================
//										INIT FUNCTION	

int VS_Init() {
	// Initialize Flags and Control Registers
	
	VS_FLAGS.SYSTEM_EXIT = 0;
	VS_FLAGS.WAKE = 0;
	VS_FLAGS.TASK_SET_ONGOING = 0;
	CR_FSETTING = MAX_FREQUENCY;
	CR_VSETTING = MAX_VOLTAGE;
	CR_TASK_SET = 0x00000000;
	CR_DEADLINE = 0;
	CR_TASK_INDEX = 0;
	CR_TASK_INDEX_END = 0;			
	CR_VF_INDEX = 0;
	
	VF_PAIR_ENTRIES = 	0;
	TASK_ENTRIES 	 =	0;
	TASK_SET_ENTRIES=	0;
	
	// Set lookup tables
	
	// Task Set Table
	
	VS_AddTaskSet(TASK_SET_RECEIVE,200);
	VS_AddTaskSet(TASK_SET_TRANSMIT,500);
	
	// Task Table
	
	VS_AddTask(dummy_FECR,100,5,0x01);
	VS_AddTask(dummy_CRCR,200,2,0x01);
	VS_AddTask(dummy_MICR,300,1,0x01);
	VS_AddTask(dummy_MICT,300,1,0x02);
	VS_AddTask(dummy_CRCT,200,2,0x02);
	VS_AddTask(dummy_FECT,100,5,0x02);
	#ifdef INTEGRATE
	VS_AddTaskSet(TASK_SET_SWITCH,500);
	VS_AddTask(switch_message,100,5,0x03);
	#endif
	import_FVRS_Settings();
	

	#ifdef BLIND
	VS_BlindInit();
	#endif
	
	return 0;
}

//===================================================================================================
//										ADD LOOKUP TABLE ENTRY FUNCTIONS	

int VS_AddTaskSet(uint8_t taskSetID, uint16_t deadline){
	Task_Sets[TASK_SET_ENTRIES].taskSetID = taskSetID;
	Task_Sets[TASK_SET_ENTRIES].deadline = deadline;
	PRINTF(("Task_Sets[%d].taskSetID = 0x%x .deadline = %u\n",
		TASK_SET_ENTRIES,
		Task_Sets[TASK_SET_ENTRIES].taskSetID,
		Task_Sets[TASK_SET_ENTRIES].deadline
	));
	return TASK_SET_ENTRIES++;
}


int VS_AddTask(int (*func)(void*), uint16_t C0, uint16_t C1, uint8_t taskSet){
	Task_Pairs[TASK_ENTRIES].taskID = func;
	Task_Pairs[TASK_ENTRIES].C0 = C0;
	Task_Pairs[TASK_ENTRIES].C1 = C1;
	Task_Pairs[TASK_ENTRIES].taskSet = taskSet;
	PRINTF(("Task[%d]: C0:%d C1:%d taskSet:0x%x\n",
		TASK_ENTRIES,
		Task_Pairs[TASK_ENTRIES].C0,
		Task_Pairs[TASK_ENTRIES].C1,
		Task_Pairs[TASK_ENTRIES].taskSet
	));
	return TASK_ENTRIES++;
}


int VS_AddVFPair(	uint16_t frequency, 
					uint16_t voltage, 
					uint16_t relativeFrequency, 
					uint16_t upVoltageSlew, 
					uint16_t downVoltageSlew) {
					
	FVRS_Table[VF_PAIR_ENTRIES].frequencyCode = frequency;
	FVRS_Table[VF_PAIR_ENTRIES].voltageCode = voltage;
	FVRS_Table[VF_PAIR_ENTRIES].relativeFrequency = relativeFrequency;
	FVRS_Table[VF_PAIR_ENTRIES].upVoltageSlew = upVoltageSlew;
	FVRS_Table[VF_PAIR_ENTRIES].downVoltageSlew = downVoltageSlew;
			
	PRINTF(("VF Pair[%d] F:0x%x V:0x%x R:0x%x uS:%u dS:%u\n",
		VF_PAIR_ENTRIES,
		FVRS_Table[VF_PAIR_ENTRIES].frequencyCode,
		FVRS_Table[VF_PAIR_ENTRIES].voltageCode,
		FVRS_Table[VF_PAIR_ENTRIES].relativeFrequency,
		FVRS_Table[VF_PAIR_ENTRIES].upVoltageSlew,
		FVRS_Table[VF_PAIR_ENTRIES].downVoltageSlew
	));

	return VF_PAIR_ENTRIES++;
}

//===================================================================================================
//										IMPORT FVRS FUNCTION	

int import_FVRS_Settings() {
	#ifdef FILEIO
	FILE* vfp;
	char buf[80];
	char temp[80];
	int i;
	uint16_t frequency;
	uint16_t voltage;
	uint16_t relative;
	uint16_t upslew;
	uint16_t downslew;
	
	printf("VF pair settings file: ");
	scanf("%s",buf);
	vfp = fopen(buf,"r");
	
	while (fgets(buf,80, (FILE*)vfp )) {
		for (i = 0; i < 4; i++) {
			temp[i] = buf[i];
		}
		temp[4] = '\0';		
		frequency = (uint16_t)strtol(temp,NULL,16);
				
		for (i = 4; i < 8; i++) {
			temp[i-4] = buf[i];
		}
		temp[4] = '\0';		
		voltage = (uint16_t)strtol(temp,NULL,16);
		
		for (i = 8; i < 12; i++) {
			temp[i-8] = buf[i];
		}
		temp[4] = '\0';		
		relative = (uint16_t)strtol(temp,NULL,16);
		
		for (i = 12; i < 16; i++) {
			temp[i-12] = buf[i];
		}
		temp[4] = '\0';		
		upslew = (uint16_t)strtol(temp,NULL,16);
		
		for (i = 16; i < 20; i++) {
			temp[i-16] = buf[i];
		}
		temp[4] = '\0';		
		downslew = (uint16_t)strtol(temp,NULL,16);
		
		VS_AddVFPair(frequency,voltage,relative,upslew,downslew);
	}
	
	fclose(vfp);
	
	#else
	// HARD CODE VF PAIRS HERE FOR WITHOUT FILE IO
	VS_AddVFPair(0x8080,0x3300,0x0a01,0x0000,0x000a);
	VS_AddVFPair(0x7070,0x3000,0x0f02,0x0005,0x000a);
	VS_AddVFPair(0x6060,0x2000,0x0501,0x0005,0x0008);
	VS_AddVFPair(0x5050,0x1000,0x0301,0x0004,0x0008);
	VS_AddVFPair(0x4040,0x0900,0x0101,0x0004,0x0000);
	
	#endif
	return 0;
}


//===================================================================================================
//										BLIND COMMANDS INITIALIZATION

#ifdef BLIND
int VS_AddBlindCommand(int index, char c, uint16_t length, uint16_t deadline) { 
	blind_commands[index].c = c;
	blind_commands[index].length = length;
	blind_commands[index].deadline = deadline;
	return 0;
}

int VS_BlindInit() {
	VS_AddBlindCommand(0,'r',50,200);
	VS_AddBlindCommand(1,'t',100,500);
	VS_AddBlindCommand(2,'r',100,200);
	VS_AddBlindCommand(3,'t',240,500);
	VS_AddBlindCommand(4,'x',1,1);
	return 0;
}
#endif
















