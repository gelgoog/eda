
#include "vsched.h"

//===================================================================================================
//										MSP INITIALIZATION	
#ifdef MSP
int _system_pre_init(void) {
	// stop WDT
	WDTCTL = WDTPW + WDTHOLD;
	// Perform C/C++ global data initialization
	return 1;
}
#endif



//===================================================================================================
//										MAIN FUNCTION	


int main(void) {
	
	VS_Init(); 
	
	while (!VS_FLAGS.SYSTEM_EXIT) {
		if (!VS_FLAGS.WAKE) VS_Sleep();
		if (VS_FLAGS.SYSTEM_EXIT) break;
		VS_SelectTask();
	} 
	return 0;
}

