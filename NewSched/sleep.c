#include "vsched.h"

int VS_Sleep() {
	
	// set frequency and voltage to the lowest setting
	VS_ChangeVF(SLEEP_MODE_VOLTAGE,SLEEP_MODE_FREQUENCY);

	CR_TASK_SET = 0;
	CR_PACKET_LENGTH = 0;
	
	#ifdef MSP
	do {	
		// nop		
	} while (!VS_FLAGS.WAKE);
	#endif
	
	
	#ifdef FILEIO
	char inputc[80];
	#ifdef DYNAMIC_DEADLINE
	uint16_t inputd;
	int offset;
	#endif
	char buf[80];
	char temp[80];
	uint16_t inputi;
	static int begin = 0;
	int i;
	
	
	if(!begin) {
		printf("Welcome to Voltage Scheduler v2.0 Simulation mode!\n");
		printf("Input File Name: ");
		scanf("%s",buf);		
		fp = fopen(buf,"r");
		printf("Log File Name: ");
		scanf("%s",buf);
		lp = fopen(buf,"w");
		begin = 1;
	}
	do{
	//----------------------------------------------------------------------------------------------
	printf("\n====================================================================");
	printf("\nTask Set: SLEEP Time: %d \tF: 0x%x V:0x%x\n> ",VS_GetTime(),REG_FREQUENCY,REG_VOLTAGE);
	
	if (fgets(buf,80, (FILE*)fp )) {
		printf("%s",buf);
		inputc[0] = buf[0];
		#ifndef DYNAMIC_DEADLINE
		for (i = 2; i < 80; i++) {
			if (buf[i] == '\n') {
				temp[i-2] = '\0';
				break;
			}
			temp[i-2] = buf[i];
		}
		inputi = atoi(temp);
		#else
		for (i = 2; buf[i] != ' '; i++) {
			temp[i-2] = buf[i];
			temp[i-1] = '\0';
		}
		offset = i + 1;
		inputi = atoi(temp);
		for (i = offset; i < 80; i++) {
			if (buf[i] == '\n') {
				temp[i-offset] = '\0';
				break;
			}
			temp[i-offset] = buf[i];
		}
		inputd = atoi(temp);
		inputd_helper = inputd;
		#endif
		
	} else {
		inputc[0] = 'x';
		inputi = 1;
	}
	
	inputi_helper = inputi;
	
	//---------------------------INPUTS---------------------------------------
	if(inputc[0] == 'x') {							// system exit
		fclose(fp);
		fclose(lp);
		int_exit();
	}
	else if(inputc[0] == 'w') {						// wait while sleeping
		currTime+= inputi;
	}
	else if(inputc[0] == 'r') {						// receive interrupt
		int_receive();
	}
	else if(inputc[0] == 't') {						// transmit interrupt
		int_transmit();
	}
	else if(inputc[0] == 's') {
		int_switch();								// switch message interrupt
	}
	else {
		printf("SYNTAX ERROR! \nusage: [w/r/t/x] [packet length]\n");
	}
	//----------------------------------------------------------------------------------------------
	}while (!VS_FLAGS.WAKE);
	
	#endif

	#ifdef BLIND
	static int i = 0;
	
	if ( blind_commands[i].c == 'r' ) {
		CR_TASK_SET = TASK_SET_RECEIVE;
	} else if (blind_commands[i].c == 't' ) {
		CR_TASK_SET = TASK_SET_TRANSMIT;
	} else if (blind_commands[i].c == 's' ) {
		CR_TASK_SET = TASK_SET_SWITCH;
	} else {
		VS_FLAGS.SYSTEM_EXIT = 1;
	}
	VS_FLAGS.WAKE = 1;
	CR_PACKET_LENGTH = blind_commands[i].length;
		#ifdef DYNAMIC_DEADLINE
		CR_DEADLINE = blind_commands[i].deadline;
		#endif
	i++;

	#endif


	return 0;
}
