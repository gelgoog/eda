#include "vsched.h"

//===================================================================================================
//										SELECT TASK FUNCTION	
int VS_SelectTask() {
	uint8_t task_fail;
	
	
	// if this is the first task call, prep the scheduler to execute the whole set sequentially
	if ( !VS_FLAGS.TASK_SET_ONGOING ) {	
		VS_StartTime();
		VS_TaskSetInit();		
		#ifdef RATE_MONOTONIC
		VS_Rate_Monotonic();
		#endif
		VS_FLAGS.TASK_SET_ONGOING = 1;	
	}
	#ifdef SIM
	PRINTF(("\n===================================================================="));
	PRINTF(("\nTask Set: 0x%x Time: %u Deadline: %u Counter: %u to %u\n",
		CR_TASK_SET,
		currTime,
		CR_DEADLINE,
		CR_TASK_INDEX,
		CR_TASK_INDEX_END
	));
	#endif
	// set the VF pair based on an algorithm
	#ifdef CYCLE_CONSERVING
	VS_Cycle_Conserving();
	#endif
	
	// do the thing
	task_fail = (*(Task_Pairs[CR_TASK_INDEX].taskID))(VS_TASK_ARG);
	CR_TASK_INDEX++;
	
	#ifdef SIM
	if(task_fail) PRINTF(("\nTASK FAILED. GOING BACK TO SLEEP\n\n"));
	#endif
	
	if ( (CR_TASK_INDEX == CR_TASK_INDEX_END) || task_fail) {		
		VS_FLAGS.WAKE = 0;
		CR_TASK_INDEX = 0;
		CR_TASK_INDEX_END = 0;
		CR_VF_INDEX = 0;
		CR_DEADLINE = 0;
		VS_FLAGS.TASK_SET_ONGOING = 0;
	}
	
	return 0;
}

//===================================================================================================
//										TASK SET INITIALIZE FUNCTION	

int VS_TaskSetInit() {
		int i;
	
		#ifndef DYNAMIC_DEADLINE
		// find the task set deadline
	
		for (i=0; i<TASK_SET_ENTRIES; i++) {
			if (Task_Sets[i].taskSetID == CR_TASK_SET) {
				CR_DEADLINE = Task_Sets[i].deadline + VS_GetTime();
				break;
			}
		}
		#endif
		// determine the start function
		
		for (i=0; i<TASK_ENTRIES; i++) {
			if (Task_Pairs[i].taskSet == CR_TASK_SET) {
				CR_TASK_INDEX = i;
				break;
			}		
		}
		
		// determine the end function
		
		for (i=CR_TASK_INDEX; i<TASK_ENTRIES; i++){
			
			if (Task_Pairs[i].taskSet != CR_TASK_SET) {
				CR_TASK_INDEX_END = i;
				break;
			}
			if (i == TASK_ENTRIES - 1) {
				CR_TASK_INDEX_END = TASK_ENTRIES;
				break;
			}
		}
		
		

	return 0;
}

//===================================================================================================
//										CYCLE CONSERVING ALGORITHM PROPER
#ifdef CYCLE_CONSERVING
int VS_Cycle_Conserving() {
	uint8_t i;
	uint8_t source_index;
	uint16_t remaining_time;
	uint16_t worst_case_sum = 0;
	uint16_t chosen_f;
	uint16_t chosen_v;
	
	// Get the sum of the worst case cycles of the remaining tasks in the task set
	
	for (i = CR_TASK_INDEX; i < CR_TASK_INDEX_END; i++) {
		worst_case_sum += Task_Pairs[i].C0 + (Task_Pairs[i].C1 * CR_PACKET_LENGTH);	
	}
	
	// Get the remaining time before the deadline
	
	remaining_time = CR_DEADLINE - VS_GetTime();

	// Get the index of the current voltage setting (for slew later)

	for (i = CR_VF_INDEX; i < VF_PAIR_ENTRIES; i++ ) {
		if (FVRS_Table[i].voltageCode == REG_VOLTAGE) {
			source_index = i;
			break;
		}
	} 
	
	// Determine the lowest-frequency VF pair which will meet the deadline
	
	PRINTF(("Sum of CC: %u Remaining Time: %u\n",worst_case_sum,remaining_time));
	
	for (i = CR_VF_INDEX; i < VF_PAIR_ENTRIES; i++) {
		PRINTF(("evaluating[%d] F %x, rtime*rf %u >= cc + slew %u? ",
			i,
			FVRS_Table[i].frequencyCode,	
			VS_Multiply(remaining_time,FVRS_Table[i].relativeFrequency),
			worst_case_sum + VS_GetSlew(source_index,i)				
		));

		// Add the projected voltage slew time to the worst case cycles

		
		if ( 	VS_Multiply(remaining_time,FVRS_Table[i].relativeFrequency) >= 
				( worst_case_sum + VS_GetSlew(source_index,i)  ) 
		   )
		{		
			PRINTF (("TRUE\n"));
			chosen_f = FVRS_Table[i].frequencyCode;
			chosen_v = FVRS_Table[i].voltageCode;
		} else {
			PRINTF (("FALSE\n"));
			if (i == 0) {
				PRINTF (("\nWARNING! TASK UNSCHEDULABLE UNDER HIGHEST FREQUENCY\n"));
				CR_VF_INDEX = 0;
				chosen_v = MAX_VOLTAGE;
				chosen_f = MAX_FREQUENCY;				
				break;
			}
			CR_VF_INDEX = i - 1;
			break;
		}
	}
	
	VS_ChangeVF(chosen_v,chosen_f);
	PRINTF(("Changed VF to V:%x F:%x\n",REG_VOLTAGE,REG_FREQUENCY));
	#ifdef FILEIO
	fprintf(lp,"%x,%u,%x,%x\n",
			CR_TASK_SET,
			CR_TASK_INDEX,
			REG_VOLTAGE,
			REG_FREQUENCY
	);
	#endif
	

	return 0;
}
#endif

//===================================================================================================
//										STATIC RATE MONOTONIC ALGORITHM PROPER
#ifdef RATE_MONOTONIC
int VS_Rate_Monotonic() {

	uint8_t i;
	uint16_t remaining_time;
	uint16_t worst_case_sum = 0;
	uint16_t chosen_f;
	uint16_t chosen_v;
	
	// Get the sum of the worst case cycles of the tasks in the task set
	
	for (i = CR_TASK_INDEX; i < CR_TASK_INDEX_END; i++) {
		worst_case_sum += Task_Pairs[i].C0 + (Task_Pairs[i].C1 * CR_PACKET_LENGTH);	
	}
	
	// Get the remaining time before the deadline
	
	remaining_time = CR_DEADLINE - VS_GetTime();
	
	// Determine the lowest-frequency VF pair which will meet the deadline
	
	PRINTF(("Sum of CC: %u Remaining Time: %u\n",worst_case_sum,remaining_time));
	
	for (i = 0; i < VF_PAIR_ENTRIES; i++) {
		PRINTF(("evaluating[%d] F %x, rtime*rf %u >= cc + slew %u? ",
			i,
			FVRS_Table[i].frequencyCode,	
			VS_Multiply(remaining_time,FVRS_Table[i].relativeFrequency),
			worst_case_sum + VS_GetSlew(VF_PAIR_ENTRIES-1,i)				
		));

		// Add the projected voltage slew time to the worst case cycles

		
		if ( 	VS_Multiply(remaining_time,FVRS_Table[i].relativeFrequency) >= 
				( worst_case_sum + VS_GetSlew(VF_PAIR_ENTRIES-1,i)  ) 
		   )
		{		
			PRINTF (("TRUE\n"));
			chosen_f = FVRS_Table[i].frequencyCode;
			chosen_v = FVRS_Table[i].voltageCode;
		} else {
			PRINTF (("FALSE\n"));
			if (i == 0) {
				PRINTF (("\nWARNING! TASK SET UNSCHEDULABLE UNDER HIGHEST FREQUENCY\n"));
				chosen_v = MAX_VOLTAGE;
				chosen_f = MAX_FREQUENCY;				
				break;
			}
			break;
		}
	}
	
	VS_ChangeVF(chosen_v,chosen_f);
	PRINTF(("Changed VF to V:%x F:%x\n",REG_VOLTAGE,REG_FREQUENCY));
	#ifdef FILEIO
	fprintf(lp,"%x,%u,%x,%x\n",
			CR_TASK_SET,
			CR_TASK_INDEX,
			REG_VOLTAGE,
			REG_FREQUENCY
	);
	#endif
	


	return 0;
}
#endif











