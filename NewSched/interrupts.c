#include "vsched.h"

#ifdef SIM
void int_receive () {
	VS_FLAGS.WAKE = 1;
	CR_PACKET_LENGTH = inputi_helper;
	CR_TASK_SET = TASK_SET_RECEIVE;
	#ifdef DYNAMIC_DEADLINE
	CR_DEADLINE = inputd_helper;
	#endif
}

void int_transmit() {
	VS_FLAGS.WAKE = 1;
	CR_PACKET_LENGTH = inputi_helper;
	CR_TASK_SET = TASK_SET_TRANSMIT;
	#ifdef DYNAMIC_DEADLINE
	CR_DEADLINE = inputd_helper;
	#endif
}

void int_exit() {
	VS_FLAGS.WAKE = 1;
	VS_FLAGS.SYSTEM_EXIT = 1;
}

void int_switch() {
	VS_FLAGS.WAKE = 1;
	CR_TASK_SET = TASK_SET_SWITCH;
}

#endif
