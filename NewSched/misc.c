#include "vsched.h"


//===================================================================================================
//										CHANGE VF FUNCTION	

int VS_ChangeVF(uint16_t voltage,uint16_t frequency){					// Changes the VF Control registers one bit at a time

	// change voltage register
	REG_VOLTAGE = voltage;
	REG_FREQUENCY = frequency;
	return 0;
}

//===================================================================================================
//										VS MULTIPLY FUNCTION

uint16_t VS_Multiply(uint16_t value, uint16_t relative){
	uint16_t result = value * (relative >> 8);
	result = result / (relative & 0x00ff);
	return result;	
}

//===================================================================================================
//										GET SLEW TIME FUNCTION
// Gets the slew time to transition from one voltage to another given their indexes in the FVRS table
// NOTE: higher index -> lower voltage

uint16_t VS_GetSlew(uint8_t source_index, uint8_t dest_index) {
	int i = source_index;
	uint16_t total = 0;

	if (source_index > dest_index) {		// if current voltage less than target voltage
		while (i != dest_index) {
			total += FVRS_Table[i].upVoltageSlew;
			i--;
		}
	} else if (dest_index > source_index) {	// if current voltage greater than target voltage
		while (i != dest_index) {
			total += FVRS_Table[i].downVoltageSlew;
			i++;
		}
	} else {								// if both voltages equal
		total = 0;
	}
	return total;	
}
