//#include <msp430.h>
#include <stdio.h>				// #ifdef this for quick un-debugging
#include <stdint.h>				// only copy the functions you need here

#include <stdlib.h>				// only for atoi in automatic testing

//#include "../eda.h"
#include "../edatest.h"

// VERY EARLY BUILD OF DVS SCHEDULER PROGRAM IN C
// uses Cycle-conserving scheduling algorithm


//===================================================================================================
//										MACROS

#define VERBOSE

#ifdef VERBOSE
#define PRINTF(x) printf x
#else
#define PRINTF(x) do {} while (0)
#endif


#define MAX_VF_PAIR_ENTRIES		10
#define MAX_TASK_ENTRIES		10
#define MAX_TASK_SET_ENTRIES	10



volatile int VF_PAIR_ENTRIES;
volatile int TASK_ENTRIES;
volatile int TASK_SET_ENTRIES;

#define TASK_SET_RECEIVE	0x01
#define TASK_SET_TRANSMIT	0x02
#define TASK_SET_SWITCH		0x03
#define SLEEP_MODE_VF		FVRS_Table[VF_PAIR_ENTRIES-1].VF //VF_Pairs[VF_PAIR_ENTRIES-1].VF
#define MAX_FREQUENCY_VF	FVRS_Table[0].VF //VF_Pairs[0].VF
												

//===================================================================================================
//										GLOBAL FLAGS (These should be implemented as control registers)												

volatile uint8_t VS_FLAG_TASK_FINISHED;		// flag for when a process has finished execution 
volatile uint8_t VS_FLAG_SYSTEM_EXIT;		// flag for when the sensor must shut down
volatile uint8_t VS_FLAG_WAKE;				// flag for waking the processor up
volatile uint8_t VS_FLAG_TASK_SET_ONGOING;	// flag for if a task set is currently ongoing

volatile uint32_t CR_VF_SETTING;				// control register for selected VF pair
volatile uint8_t CR_TASK_SET;					// control register for current task set
volatile uint16_t CR_PACKET_LENGTH;				// control register for packet length in bytes

volatile uint32_t CR_DEADLINE;
volatile uint8_t CR_TASK_INDEX;
volatile uint8_t CR_TASK_INDEX_END;				// when the task_counter equals this, end the task set;

workspace_t test_space;
void* VS_TASK_ARG = &test_space;

unsigned int currTime = 0;				// TEST variable for number of elapsed time units
FILE *fp;								// For automated inputs
FILE *lp;								// For automated VF logging


//===================================================================================================
//										FUNCTIONS												

int VS_Init();								// Initializes all the lookup tables 
int VS_Sleep();								// Sets Voltage and Frequency to minimum until woken up
int VS_SelectTask();						// Selects the next task
int VS_ChangeVF(uint32_t);					// Changes the VF Control register one bit at a time
int VS_TaskSetInit();						// Prepares the scheduler to run a whole task set
int VS_Cycle_Conserving();					// Selects the next operating point based on the task

int VS_AddTask(int (*func)(void*), uint16_t, uint16_t, uint8_t );
				// function name,C0,C1,TaskSet
int VS_AddTaskSet(uint8_t, uint32_t);
				// taskSetID, deadline
int VS_AddVFPair(uint32_t,uint16_t);
				// VF, relativeFrequency
int VS_AddFVRSPair(uint64_t);
int import_VF_Settings();
int import_FVRS_Settings();

unsigned int VS_Multiply(unsigned int,uint16_t);		// Quick multiply by values: 0 < x < 1

// These functions are PROCESSOR-SPECIFIC :)

int VS_StartTime();							// Starts 
int VS_GetTime();							// Gives the current time in Time Units
int VS_StopTime();


//	TEST FUNCTIONS
//  These are the functions ian and al are working on. They may freely add or subtract more functions
//  Doesn't matter what the function names are, as long as they return int and have 1 void* argument

int dummy_FECR(void*);
int dummy_CRCR(void*);
int dummy_MICR(void*);
int dummy_FECT(void*);
int dummy_CRCT(void*);
int dummy_MICT(void*);
int msg_init();
int switch_message(void*);
int dummy_Default(int,char[]);


//===================================================================================================
//										LOOKUP TABLES					

union FVRS_Table {
	uint64_t FVRS;
	
	
	struct {
		uint16_t voltageSlew;
		uint16_t relativeFrequency;
	
		union {
			uint32_t VF;
			struct {
				uint16_t voltageCode;
				uint16_t frequencyCode;
			};
		};
	};
	
}FVRS_Table[MAX_VF_PAIR_ENTRIES];


union VF_Pairs {
	uint32_t VF;
	struct VF_Pair {
		uint16_t voltageCode;
		uint16_t frequencyCode;
	} VF_Pair;
}VF_Pairs[MAX_VF_PAIR_ENTRIES];


struct Task_Pairs {
	int (*taskID) (void*);
	uint16_t C0;
	uint16_t C1;
	uint8_t taskSet;
} Task_Pairs[MAX_TASK_ENTRIES];

struct Task_Sets {
	uint32_t deadline;
	uint8_t taskSetID;
} Task_Sets[MAX_TASK_SET_ENTRIES];
/*
	HOW TO USE:
	Task_Pairs[0].taskID = dummy_FECR;			// assigns taskID to a pointer to a function
	printf("%p\n",Task_Pairs[0].taskID);		// use %p for printf in case of debug
	(*(Task_Pairs[0].taskID))();				// this is how to call a function from the lookup table!
*/
union Relative_Frequencies {
	uint32_t RF;
	struct RF_Pair {
		uint16_t relativeFrequency;
		uint16_t frequencyCode;
	} RF_Pair;
} Relative_Frequencies[MAX_VF_PAIR_ENTRIES];


//===================================================================================================
//										MAIN FUNCTION				

int main (void) {
	
	// Insert Processor-specific initialization here
	
	VS_Init(); 
	
	while (!VS_FLAG_SYSTEM_EXIT) {
		while (!VS_FLAG_WAKE) {
			VS_Sleep();
		}
		if (VS_FLAG_SYSTEM_EXIT) break;
		VS_SelectTask();
	} 
	
	PRINTF(("SYSTEM EXIT\n"));
	
	return 0;
}

//===================================================================================================
//										INIT FUNCTION		

		/*		FOR TESTING PURPOSES
				V-F pairs
					F					V
					0x8080	[10 MHz]	0x3300
					0x7070	[7.5 MHz]	0x3000
					0x6060	[5 MHz]		0x2000
					0x5050	[3 MHz]		0x1000 
					0x4040	[1 MHz]		0x0900			(The last entry will determine the sleep settings) 
				Tasks						
					FUNCTION			C0		C1		TASKSET			(Worst case comp cycles = C0 + B*C1 where
					FECR (Receive FEC)	100		5		0x01 (R)			B is the number of input bits
					CRCR (Receive CRC)	200		2		0x01 (R)
					MICR (Receive MIC)	300		1		0x01 (R)
					MICT (Transmit MIC) 300		1		0x02 (T)
					CRCT (Transmit CRC) 200		2		0x02 (T)
					FECT (Transmit FEC)	100		5		0x02 (T)
				TaskSets
					TASKSET		DEADLINE
					0x01 (R)	2000
					0x02 (T)	5000
				Relative Frequencies
					FrequencyID			Relative Value (Binary rep)
					0x8080	[10MHz]		0000 0000 0000 0000
					0x7070	[7.5MHz]	1100 0000 0000 0000
					0x6060	[5MHz]		1000 0000 0000 0000
					0x5050	[3MHz]		0100 1100 1100 1100
					0x4040	[1MHz]		0001 1001 1001 1001			(assume the reference clock is 1MHz)
		*/


int VS_Init() {
	// Initialize Flags and Control Registers
	
	// On the MSP430 this is where we would do all the housekeeping
	VS_FLAG_TASK_FINISHED = 0;
	VS_FLAG_SYSTEM_EXIT = 0;
	VS_FLAG_WAKE = 0;
	VS_FLAG_TASK_SET_ONGOING = 0;
	CR_VF_SETTING = 0x00000000;
	CR_TASK_SET = 0x00000000;
	CR_DEADLINE = 0;
	CR_TASK_INDEX = 0;
	CR_TASK_INDEX_END = 0;			
	
	VF_PAIR_ENTRIES = 	0;
	TASK_ENTRIES 	 =	0;
	TASK_SET_ENTRIES=	0;
	
	// Set lookup tables
	
	// Task Set Table
	
	VS_AddTaskSet(0x01,2000);
	VS_AddTaskSet(0x02,5000);
	VS_AddTaskSet(0x03,5000);
	
	// Task Table
	
	VS_AddTask(dummy_FECR,100,5,0x01);
	VS_AddTask(dummy_CRCR,200,2,0x01);
	VS_AddTask(dummy_MICR,300,1,0x01);
	VS_AddTask(dummy_MICT,300,1,0x02);
	VS_AddTask(dummy_CRCT,200,2,0x02);
	VS_AddTask(dummy_FECT,100,5,0x02);
	VS_AddTask(switch_message,100,5,0x03);
	
	// RF and VF Tables
	
	// VS_AddFVRSPair(0x123456789abcdef0);
	
	//import_VF_Settings();
	import_FVRS_Settings();
	
/*	VS_AddVFPair(0x80803300,0x0000);
	VS_AddVFPair(0x70703000,0xc000);
	VS_AddVFPair(0x60602000,0x8000);
	VS_AddVFPair(0x50501000,0x4ccc);
	VS_AddVFPair(0x40400900,0x1999);*/
	
	msg_init();		// initializes the test message for al's code
	
	
	return 0;
}	

//===================================================================================================
//										IMPORT SETTINGS FUNCTIONS	


int VS_AddTask(int (*func)(void*), uint16_t C0, uint16_t C1, uint8_t taskSet) {
	Task_Pairs[TASK_ENTRIES].taskID = func;
	Task_Pairs[TASK_ENTRIES].C0 = C0;
	Task_Pairs[TASK_ENTRIES].C1 = C1;
	Task_Pairs[TASK_ENTRIES].taskSet = taskSet;
	PRINTF(("Task[%d]: C0:%d C1:%d taskSet:0x%x\n",
		TASK_ENTRIES,
		Task_Pairs[TASK_ENTRIES].C0,
		Task_Pairs[TASK_ENTRIES].C1,
		Task_Pairs[TASK_ENTRIES].taskSet));
	return TASK_ENTRIES++;
}

int VS_AddFVRSPair(uint32_t VF, uint16_t RF, uint16_t VS) {
	FVRS_Table[VF_PAIR_ENTRIES].VF = VF;
	FVRS_Table[VF_PAIR_ENTRIES].relativeFrequency = RF;
	FVRS_Table[VF_PAIR_ENTRIES].voltageSlew = VS;
	
	
	PRINTF(("VF Pair[%d] VF:%lx F:%x V:%x R:%x S:%x\n",
		VF_PAIR_ENTRIES,
		FVRS_Table[VF_PAIR_ENTRIES].VF,
		FVRS_Table[VF_PAIR_ENTRIES].frequencyCode,
		FVRS_Table[VF_PAIR_ENTRIES].voltageCode,
		FVRS_Table[VF_PAIR_ENTRIES].relativeFrequency,
		FVRS_Table[VF_PAIR_ENTRIES].voltageSlew
	));

	return VF_PAIR_ENTRIES++;
}

int VS_AddVFPair(uint32_t VF, uint16_t relativeFrequency) {
	VF_Pairs[VF_PAIR_ENTRIES].VF = VF;
	Relative_Frequencies[VF_PAIR_ENTRIES].RF_Pair.frequencyCode = VF_Pairs[VF_PAIR_ENTRIES].VF_Pair.frequencyCode;
	Relative_Frequencies[VF_PAIR_ENTRIES].RF_Pair.relativeFrequency = relativeFrequency;
	PRINTF(("VF[%d]: %lx RF: 0x%lx\n",
		VF_PAIR_ENTRIES,
		VF_Pairs[VF_PAIR_ENTRIES].VF,
		Relative_Frequencies[VF_PAIR_ENTRIES].RF));
	return VF_PAIR_ENTRIES++;
}

int VS_AddTaskSet(uint8_t taskSetID, uint32_t deadline) {
	Task_Sets[TASK_SET_ENTRIES].taskSetID = taskSetID;
	Task_Sets[TASK_SET_ENTRIES].deadline = deadline;
	return TASK_SET_ENTRIES++;
}

int import_VF_Settings() {
	FILE* vfp;
	char buf[128];
	char temp[80];
	int i;
	uint32_t vf;
	uint16_t rf;
	
	printf("VF pair settings file: ");
	scanf("%s",buf);
	vfp = fopen(buf,"r");
	
	while (fgets(buf,128, (FILE*)vfp )) {
	
		if (buf[0] == '#'|| buf[0] == '\n') continue;
	
		for (i = 0; buf[i] != ' '; i++) {
			temp[i] = buf[i];
		}		
		vf = (uint32_t)strtol(temp,NULL,16);		
		for (i = 9; i < 13; i++) {
			temp[i-9] = buf[i];
		}
		temp[4] = '\0';		
		rf = (uint16_t)strtol(temp,NULL,16);
		VS_AddVFPair(vf,rf);
	}
	
	
	fclose(vfp);
	
	return 0;
}

int import_FVRS_Settings(){
	FILE* vfp;
	char buf[128];
	char temp[128];
	int i;
	
	uint32_t vf;
	uint16_t rf;
	uint16_t vs;
	
	printf("VF pair settings file: ");
	scanf("%s",buf);
	vfp = fopen(buf,"r");
	
	while (fgets(buf,128, (FILE*)vfp )) {
		if (buf[0] == '#' || buf[0] == '\n') continue;
		
		for (i = 0; i < 8; i++) {
			temp[i] = buf[i];
		}
		temp[8] = '\0';
		
		vf = (uint32_t)strtol(temp,NULL,16);
		
		for (i = 8; i < 12; i++) {
			temp[i-8] = buf[i];
		}
		temp[4] = '\0';
		
		rf = (uint16_t)strtol(temp,NULL,16);
		
		for (i = 12; i < 16; i++) {
			temp[i-12] = buf[i];
		}
		temp[4] = '\0';
		
		vs = (uint16_t)strtol(temp,NULL,16);
		
		VS_AddFVRSPair(vf,rf,vs);
	}
	
	fclose(vfp);
	return 0;
}

//===================================================================================================
//										SELECT TASK FUNCTION	

int VS_SelectTask() {
	uint8_t task_fail;
	
	
	// if this is the first task call, prep the scheduler to execute the whole set sequentially
	if ( !VS_FLAG_TASK_SET_ONGOING ) {	
		VS_TaskSetInit();		
		VS_FLAG_TASK_SET_ONGOING = 1;	
	}
	
	printf("\n====================================================================");
	printf("\nTask Set: 0x%x Time: %u Deadline: %lu Counter: %u to %u\n",CR_TASK_SET,currTime,CR_DEADLINE,CR_TASK_INDEX,CR_TASK_INDEX_END);
	
	// set the VF pair based on an algorithm
	VS_Cycle_Conserving();
	
	// do the thing
	task_fail = (*(Task_Pairs[CR_TASK_INDEX].taskID))(VS_TASK_ARG);
	CR_TASK_INDEX++;
	
	if(task_fail) printf("\nTASK FAILED. GOING BACK TO SLEEP\n\n");
	
	
	if ( (CR_TASK_INDEX == CR_TASK_INDEX_END) || task_fail) {		
		VS_FLAG_WAKE = 0;
		CR_TASK_INDEX = 0;
		CR_TASK_INDEX_END = 0;
		CR_DEADLINE = 0;
		VS_FLAG_TASK_SET_ONGOING = 0;
	}
	
	return 0;
}


//===================================================================================================
//										TASK SET INITIALIZE FUNCTION	

int VS_TaskSetInit() {
		int i;
	
		// find the task set deadline
	
		for (i=0; i<TASK_SET_ENTRIES; i++) {
			if (Task_Sets[i].taskSetID == CR_TASK_SET) {
				CR_DEADLINE = Task_Sets[i].deadline + VS_GetTime();
				break;
			}
		}
		
		// determine the start function
		
		for (i=0; i<TASK_ENTRIES; i++) {
			if (Task_Pairs[i].taskSet == CR_TASK_SET) {
				CR_TASK_INDEX = i;
				break;
			}		
		}
		
		// determine the end function
		
		for (i=CR_TASK_INDEX; i<TASK_ENTRIES; i++){
			
			if (Task_Pairs[i].taskSet != CR_TASK_SET) {
				CR_TASK_INDEX_END = i;
				break;
			}
			if (i == TASK_ENTRIES - 1) {
				CR_TASK_INDEX_END = TASK_ENTRIES;
				break;
			}
		}
		


	return 0;
}

//===================================================================================================
//										SLEEP FUNCTION	

int VS_Sleep() {
	
	char inputc[80];
	char buf[80];
	char temp[80];
	unsigned int inputi;
	static int begin = 0;
	int i;
	
	
	if(!begin) {
		printf("Welcome to Voltage Scheduler v1.0 Simulation mode!\n");
		printf("Input File Name: ");
		scanf("%s",buf);		
		fp = fopen(buf,"r");
		printf("Log File Name: ");
		scanf("%s",buf);
		lp = fopen(buf,"w");
		begin = 1;
	}
	
	VS_ChangeVF (SLEEP_MODE_VF);	// The sleep mode setting is always the last entry of the VF pair table
	
	CR_TASK_SET = 0;
	CR_PACKET_LENGTH = 0;
	
	printf("\n====================================================================");
	printf("\nTask Set: SLEEP Time: %d \tVF: 0x%lu\n> ",VS_GetTime(),CR_VF_SETTING);
	
	if (fgets(buf,80, (FILE*)fp )) {
		printf("%s",buf);
		for (i = 2; i < 80; i++) {
			if (buf[i] == '\n') {
				temp[i-2] = '\0';
				break;
			}
			temp[i-2] = buf[i];
		}
		inputi = atoi(temp);
		inputc[0] = buf[0];
		
	} else {
		inputc[0] = 'x';
		inputi = 1;
	}
	
	//---------------------------INPUTS---------------------------------------
	if(inputc[0] == 'x') {							// system exit
		fclose(fp);
		fclose(lp);
		VS_FLAG_WAKE = 1;
		VS_FLAG_SYSTEM_EXIT = 1;
	}
	else if(inputc[0] == 'w') {						// wait while sleeping
		currTime+= inputi;
	}
	else if(inputc[0] == 'r') {						// receive interrupt
		VS_FLAG_WAKE = 1;
		CR_TASK_SET = TASK_SET_RECEIVE;	
		CR_PACKET_LENGTH = inputi;
	}
	else if(inputc[0] == 't') {						// transmit interrupt
		VS_FLAG_WAKE = 1;
		CR_TASK_SET = TASK_SET_TRANSMIT;	
		CR_PACKET_LENGTH = inputi;
	} else if(inputc[0] == 's') {
		VS_FLAG_WAKE = 1;
		CR_TASK_SET = TASK_SET_SWITCH;
		
	}
	else {
		printf("SYNTAX ERROR! \nusage: [w/r/t/s/x] [packet length]\n");
	}



	return 0;
}

//===================================================================================================
//										CYCLE CONSERVING ALGORITHM PROPER

int VS_Cycle_Conserving() {
	int i;
	uint32_t remaining_time;
	uint32_t worst_case_sum = 0;
	uint32_t selected_VF = MAX_FREQUENCY_VF;
	
	// Get the sum of the worst case times of the remaining tasks in the task set
	
	for (i = CR_TASK_INDEX; i < CR_TASK_INDEX_END; i++) {
		worst_case_sum += Task_Pairs[i].C0 + (Task_Pairs[i].C1 * CR_PACKET_LENGTH);	
	}
	
	// Get the remaining time before the deadline
	
	remaining_time = CR_DEADLINE - VS_GetTime();
	
	// Determine the lowest-frequency VF pair which will meet the deadline
	// To do this, VS_Multiply the remaining time by the relative frequency
	//		then if the result is >= the worst case cycles,
	//		then the set is schedulable under the frequency
	
	PRINTF(("Sum of CC: %lu Remaining Time: %lu\n",worst_case_sum,remaining_time));
	
	for (i = 0; i < VF_PAIR_ENTRIES; i++) {
		
		PRINTF(("evaluating VF %lx, rtime*rf %u >= cc %lu? ",FVRS_Table[i].VF,						// ...,VF_Pairs[i].VF,
			VS_Multiply(remaining_time,FVRS_Table[i].relativeFrequency),							// ...,Relative_Frequencies[i].RF_Pair.relativeFrequency),
			worst_case_sum));
		
		if ( VS_Multiply(remaining_time,FVRS_Table[i].relativeFrequency) >= worst_case_sum ) {		//, Relative_Frequencies[i].RF_Pair.relativeFrequency) >=
			selected_VF = FVRS_Table[i].VF;														// selected_VF = VF_Pairs[i].VF;
			PRINTF (("TRUE\n"));
		} else {
			PRINTF (("FALSE\n"));
			break;
		}
	}
	
	
	VS_ChangeVF(selected_VF);
	
	fprintf(lp,"T %x V %lx\n",CR_TASK_SET,selected_VF);
	
	
	
	

	return 0;
}

//===================================================================================================
//										TIME FUNCTIONS

int VS_StartTime() {
	currTime = 0;
	return 0;
}

int VS_GetTime() {
	return currTime;
}

//===================================================================================================
//										CHANGE VF REGISTER PER BIT

int VS_ChangeVF(uint32_t newVF) {
	
	//CR_VF_SETTING = newVF; 
	CR_VF_SETTING = 0;
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x80000000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x40000000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x20000000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x10000000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x08000000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x04000000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x02000000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x01000000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00800000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00400000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00200000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00100000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00080000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00040000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00020000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00010000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00008000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00004000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00002000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00001000);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000800);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000400);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000200);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000100);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000080);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000040);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000020);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000010);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000008);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000004);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000002);
	CR_VF_SETTING = CR_VF_SETTING | (newVF & 0x00000001);
	return 0;
}

//===================================================================================================
//										INTERRUPT VECTORS
//				
#ifdef MSP
__attribute__((__interrupt__(INPUTGEN_VECTOR))) 
void inputReceivedInterrupt(void)
{
	IPG0CTL = IPG0CTL & (~IPGFG);	 // cancel interrupt flag
	// set the task set control register
	if (INPUTGEN_VECTOR == INTERRUPT_RECEIVE) {		
		CR_TASK_SET = TASK_SET_RECEIVE;	
	} else if (INPUTGEN_VECTOR == INTERRUPT_TRANSMIT) {
		CR_TASK_SET = TASK_SET_TRANSMIT;
	}
	// wake the processor up
	VS_FLAG_WAKE = 1;
	CR_PACKET_LENGTH = test_space.length[0];
}
#endif



//===================================================================================================
//										VS MULTIPLY
//				

unsigned int VS_Multiply(unsigned int A, uint16_t B) {

	if (! (B > 0) ) return A;	// if B is equal to 0 then it is multiplication by 1
	
	int i;
	unsigned int result = 0;
	
	for (i=15; i>-1; i--) {
		if ( ((B >> i) & 0x0001) ) {		// if there is a 1
			result += A >> (16-i);			// shift A right and add
		}
	}
	return result;
}

//===================================================================================================
//										MESSAGE INIT

int msg_init() {

	
    size_t init_msglen, i;
    
    init_msglen = 38;
    unsigned char Message[] = {
    	0x38, 0xD5, 0x3A, 0xCE, 0xAA, 0xAF, 0x00, 0x00, 0xF4, 0x36, 0xAF, 0x97,
    	0x36, 0x33, 0x32, 0x6E, 0xE9, 0xAD, 0xDD, 0xF8, 0x75, 0x4C, 0x4E, 0x14,
    	0x47, 0x46, 0x02, 0x62, 0xC2, 0x4E, 0x6C, 0x14, 0x00, 0x00, 0x00, 0x00,
    	0x00, 0x00 };
    test_space.info = 0x80;	// Uplink
    test_space.info |= 0x08;	// sf = 8, PHDRlen = 6
    
    for (i = 0; i < init_msglen; i++)
    {
        test_space.data[i] = Message[i];
    }
    unsigned char key[16] = {0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6,
		0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C };
    for (i = 0; i < 16; i++)
    {
        test_space.key[i] = key[i];
    }
    test_space.length[1] = (init_msglen >> 8) & 0xff;
    test_space.length[0] = init_msglen & 0xff;
    test_space.output[1] = test_space.length[1] & 0x07;
    test_space.output[0] = test_space.length[0] & 0xff;



	return 0;
}
//===================================================================================================
//										DUMMY TEST FUNCTIONS

int dummy_Default(int index, char name[80]){
	uint16_t frequency = CR_VF_SETTING >> 16;
	uint16_t relativef;
	unsigned int elapsed_time = 0;
	int total_work = 1000 * (Task_Pairs[index].C0 + (CR_PACKET_LENGTH*Task_Pairs[index].C1));
	int power;//VS_Multiply(1000,Relative_Frequencies.);
	
	int i;
	
	for (i=0; i<VF_PAIR_ENTRIES; i++) {
		if (FVRS_Table[i].frequencyCode == frequency) {		//if (Relative_Frequencies[i].RF_Pair.frequencyCode == frequency) {
			relativef = FVRS_Table[i].relativeFrequency;	//relativef = Relative_Frequencies[i].RF_Pair.relativeFrequency;
			break;
		}	
	}
	
	power = VS_Multiply(1100,relativef);
	
	PRINTF (("DOING %s; work = %d frequencyID = 0x%x rF = 0x%x power = %d\n",name,total_work,frequency,relativef,power));
	
	
	while (total_work > 0) {
		currTime++;
		elapsed_time++;
		total_work -= power;	
	}

	PRINTF (("\tELAPSED TIME: %u\n",elapsed_time)); 

	return 0;
}

int dummy_FECR(void* arg) {
	int diditfail;
	workspace_t & parg = *((workspace_t *) arg);
	
    PrintWorkspace(parg, 1);
	diditfail = DecodeFEC(arg);
	printf("After DecodeFEC:\n");
    PrintWorkspace(parg, 0);

	int index = 0;
	char name[80] = "RECEIVE FEC";
	dummy_Default(index,name);
	return diditfail;
}

int dummy_CRCR(void* arg) {
	int diditfail;
	workspace_t & parg = *((workspace_t *) arg);
	
	diditfail = DecodeCRC(arg);
	printf("After DecodeCRC:\n");
    PrintWorkspace(parg, 0);

	int index = 1;
	char name[80] = "RECEIVE CRC";
	dummy_Default(index,name);
	return diditfail;
}
int dummy_MICR(void* arg) {
	int diditfail;
	workspace_t & parg = *((workspace_t *) arg);
	
	diditfail = DecodeMIC(arg);
	printf("After DecodeMIC:\n");
    PrintWorkspace(parg, 0);

	int index = 2;
	char name[80] = "RECEIVE MIC";
	dummy_Default(index,name);
	return diditfail;
}
int dummy_FECT(void* arg) {
	int diditfail;
	workspace_t & parg = *((workspace_t *) arg);
	
	diditfail = EncodeFEC(arg);
	printf("After EncodeFEC:\n");
    PrintWorkspace(parg, 0);

	int index = 5;
	char name[80] = "TRANSMIT FEC";
	dummy_Default(index,name);
	return diditfail;
}
int dummy_CRCT(void* arg) {
	int diditfail;
	workspace_t & parg = *((workspace_t *) arg);
	
	diditfail = EncodeCRC(arg);
	printf("After EncodeCRC:\n");
    PrintWorkspace(parg, 0);

	int index = 4;
	char name[80] = "TRANSMIT CRC";
	dummy_Default(index,name);
	return diditfail;
}
int dummy_MICT(void* arg) {
	int diditfail;
	workspace_t & parg = *((workspace_t *) arg);
	
    PrintWorkspace(parg, 1);
	diditfail = EncodeMIC(arg);
	printf("After EncodeMIC:\n");
    PrintWorkspace(parg, 0);
	
	int index = 3;
	char name[80] = "TRANSMIT MIC";
	dummy_Default(index,name);
	return diditfail;
}

int switch_message(void* arg) {
	size_t init_msglen, i;
	// change the test case here!
	// invoke in input file with 's 1' 
	PRINTF(("SWITCHING MESSAGES PLEASE STAND BY\n"));
	if(test_space.info == 0x08 && test_space.data[0] == 0x38)
	{
		unsigned char test_data[] = {
		0x06, 0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 0xAB, 0xF7, 0x15,
		0x88, 0x09, 0xCF, 0x4F, 0x3C, 0x44, 0x00, 0x87, 0x55, 0x00, 0xFF, 0x99,
		0x00, 0x87, 0x87, 0xAA, 0x78, 0xCC, 0x99, 0x66, 0xCC, 0x4B, 0xCC, 0x33,
		0xAA, 0x1E, 0x99, 0xCC, 0x78, 0xE1, 0x33, 0x2D, 0xE1, 0xAA, 0x1E, 0xE1,
		0xE1, 0x33, 0x78, 0xCC, 0xD2, 0x00, 0x55, 0xAA, 0x33, 0x2D, 0xFF, 0xB4,
		0x55, 0xFF, 0x2D, 0x1E, 0x99, 0x33, 0x78, 0xFF, 0xFF, 0xAA, 0xFF, 0x2D,
		0x2D, 0x4B, 0x2D, 0x4B, 0x78, 0xFF, 0xE1, 0x1E, 0xE1, 0xAA, 0x33, 0x1E,
		0x00, 0x99, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00
		};
	
		test_space.info = test_data[0];
		for (i = 0; i < 16; i++)
		{
			test_space.key[i] = test_data[i + 1];
		}
		test_space.length[0] = test_data[17];
		test_space.length[1] = test_data[18];
		for (i = 0; i < 700; i++)
		{
			test_space.data[i] = test_data[19 + i];
		}
		test_space.output[0] = test_data[719];
		test_space.output[0] = test_data[720];
	}
	else if(test_space.info & 0x80)
	{
		test_space.info &= 0x7f;	// Downlink
		init_msglen = (test_space.output[1] & 0x07) << 8;
    	init_msglen |= test_space.output[0] & 0xff;
		init_msglen -= 4; 		//Remove Payload CRC
		test_space.output[1] = (init_msglen >> 8) & 0x07;
		test_space.output[0] = init_msglen & 0xff;
		test_space.length[1] = test_space.output[1] & 0x07;
		test_space.length[0] = test_space.output[0] & 0xff;
	}
	else if(test_space.info & 0x80)
	{
		test_space.info |= 0x80;	// Uplink
		init_msglen = (test_space.output[1] & 0x07) << 8;
    		init_msglen |= test_space.output[0] & 0xff;
		init_msglen += 2; 		//Remove Payload CRC
		test_space.output[1] = (init_msglen >> 8) & 0x07;
		test_space.output[0] = init_msglen & 0xff;
		test_space.length[1] = test_space.output[1] & 0x07;
		test_space.length[0] = test_space.output[0] & 0xff;
	}
	return 0;

}
