#include <stdio.h>
#include "edatest.h"


int main()
{
	workspace_t testspace;
	workspace_t* testspace_p = &testspace;
    size_t init_msglen, i, msglen;
    
    init_msglen = 38;
    unsigned char Message[] = {
    	0x38, 0xD5, 0x3A, 0xCE, 0xAA, 0xAF, 0x00, 0x00, 0xF4, 0x36, 0xAF, 0x97,
    	0x36, 0x33, 0x32, 0x6E, 0xE9, 0xAD, 0xDD, 0xF8, 0x75, 0x4C, 0x4E, 0x14,
    	0x47, 0x46, 0x02, 0x62, 0xC2, 0x4E, 0x6C, 0x14, 0x00, 0x00, 0x00, 0x00,
    	0x00, 0x00 };
    testspace.info = 0x80;	// Uplink
    testspace.info |= 0x08;	// sf = 8, PHDRlen = 6
    
    for (i = 0; i < init_msglen; i++)
    {
        testspace.data[i] = Message[i];
    }
    unsigned char key[16] = {0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6,
		0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C };
    for (i = 0; i < 16; i++)
    {
        testspace.key[i] = key[i];
    }
    testspace.length[1] = (init_msglen >> 8) & 0xff;
    testspace.length[0] = init_msglen & 0xff;
    testspace.output[1] = 0x00;
    testspace.output[0] = 0x00;
    
    PrintWorkspace(testspace, 1);
    EncodeMIC(testspace_p);
	printf("After EncodeMIC:\n");
    PrintWorkspace(testspace, 0);
    EncodeCRC(testspace_p);
	printf("After EncodeCRC:\n");
    PrintWorkspace(testspace, 0);
    EncodeFEC(testspace_p);
	printf("After EncodeFEC:\n");
    PrintWorkspace(testspace, 0);
	
	// Now to manipulate the data to try to "decode" it.
	msglen = (testspace.output[1] << 8) & 0xff00;
	msglen |= testspace.output[0] & 0xff;
	msglen -= 4;	// Ignore the 8,4'd pCRC.
    testspace.length[1] = (msglen >> 8) & 0xff;
    testspace.length[0] = msglen & 0xff;
	
    testspace.output[1] = 0x00;
    testspace.output[0] = 0x00;
    
	int error;
    PrintWorkspace(testspace, 1);
    error = DecodeFEC(testspace_p);
	printf("After DecodeFEC: %01d\n", error);
    PrintWorkspace(testspace, 0);
    error = DecodeCRC(testspace_p);
	printf("After DecodeCRC: %01d\n", error);
    PrintWorkspace(testspace, 0);
    error = DecodeMIC(testspace_p);
	printf("After DecodeMIC: %01d\n", error);
    PrintWorkspace(testspace, 0);
	
    
    return 0;
}

/*
void PrintWorkspace(workspace_t ws, unsigned char initial_run)
{
	unsigned char decoded = (ws.output[1] & 0x80) >> 7;
	size_t i, init_msglen, msglen, sf;
	init_msglen = ((ws.length[1] & 0xff) << 8) | (ws.length[0] & 0xff);
	msglen = ((ws.output[1] & 0x07) << 8) | (ws.output[0] & 0xff);
	sf = ws.info & 0x0f;
	
	aesblock aeskey;
	for (i = 0; i < 16; i++)
	{
		aeskey.data[i] = ws.key[i];
	}
	if (initial_run)
	{
		printf("\nSF: %02d, Length: %03d\n", sf, init_msglen);
		printf("Data:\n");
		for (i = 0; i < init_msglen; i++)
		{
			if (i % 16 == 0)
			{
				printf("\n");
			}
			else if (i % 4 == 0)
			{
				printf(" ");
			}
			printf("%02x", ws.data[i]);
		}
		printf("\n");
		printf("\nAES key:\n");
		printAESblock(aeskey);
		printf("\n\n");
	}
	else
	{
		printf("\nLength: %03d, Decoded = %01d\n", msglen, decoded);
		printf("Data:");
		for (i = 0; i < msglen; i++)
		{
			if (i % 16 == 0)
			{
				printf("\n");
			}
			else if (i % 4 == 0)
			{
				printf(" ");
			}
			printf("%02x", ws.data[i]);
		}
		printf("\n");
	}
	
	return;
}
*/

/*
Test case:
38 bytes
sf = 6
data = {0x38, 0xD5, 0x3A, 0xCE, 0xAA, 0xAF, 0x00, 0x00, 0xF4, 0x36, 0xAF, 0x97, 0x36, 0x33, 0x32, 0x6E, 0xE9, 0xAD, 0xDD, 0xF8, 0x75, 0x4C, 0x4E, 0x14, 0x47, 0x46, 0x02, 0x62, 0xC2, 0x4E, 0x6C, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

aes key is:
key = {0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6,
0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C };
*/
