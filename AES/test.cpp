#include <stdio.h>
#include "aestest.h"

int main()
{
    aesblock src;
    aesblock key;
	aesblock correct;
    int i;
    /*
    for (i = 0; i < 16; i++)
    {
        src.data[i] = i;
        key.data[i] = 0;
    }
    */
    unsigned char input1[16] = {0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d,
    0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34};
    unsigned char key1[16] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
    0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
	unsigned char answer[16] = {0x39, 0x25, 0x84, 0x1d, 0x02, 0xdc, 0x09, 0xfb,
	0xdc, 0x11, 0x85, 0x97, 0x19, 0x6a, 0x0b, 0x32};
    
    for (i = 0; i < 16; i++)
    {
        src.data[i] = input1[i];
        key.data[i] = key1[i];
		correct.data[i] = answer[i];
    }
    printf("\nAES-128 test. Main function is computeAESblock.\n");
	printf("Test case taken from:\n");
	printf("Federal Information Processing Standards Publication 197\n");
	printf("Announcing the ADVANCED ENCRYPTION STANDARD (AES)\n");
	printf("(http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf)\n");
	printf("Appendix B - Cipher Example\n");

    printf("\nInitial src:\n");
    printAESblock(src);
    
    printf("\nInitial key:\n");
    printAESblock(key);
    
    src = computeAESblock(src, key);
    
    printf("\nAfter computeAESblock:\n");
    printAESblock(src);

    printf("\nCorrect result:\n");
    printAESblock(correct);
    
    /*
    unsigned char test_bytes[] = {0x57, 0xae, 0x47, 0x8e};
    for (i = 0; i < 4; i++)
    {
        printf("%02x:\t%02x\n", test_bytes[i], xtime(test_bytes[i]));
    }
    printf("%02x:\t%02x\n", test_bytes[0], xtime(test_bytes[0], 2));
    printf("%02x:\t%02x\n", test_bytes[0], xtime(test_bytes[0], 3));
    printf("%02x:\t%02x\n", test_bytes[0], xtime(test_bytes[0], 4));
    */
    
	printf("\nMIC (using AES-CMAC) test. Main function is getMIC.\n");
	printf("Test case taken from:\n");
	printf("RFC 4493 - The AES-CMAC Algorithm\n");
	printf("(https://tools.ietf.org/html/rfc4493)\n");
	printf("Page 10 Sec. 4 - Test Vectors, Example 4\n");
	
    unsigned char test_string[64] = {
        0x6b, 0xc1, 0xbe, 0xe2, 0x2e, 0x40, 0x9f, 0x96,
        0xe9, 0x3d, 0x7e, 0x11, 0x73, 0x93, 0x17, 0x2a,
        0xae, 0x2d, 0x8a, 0x57, 0x1e, 0x03, 0xac, 0x9c,
        0x9e, 0xb7, 0x6f, 0xac, 0x45, 0xaf, 0x8e, 0x51,
        0x30, 0xc8, 0x1c, 0x46, 0xa3, 0x5c, 0xe4, 0x11,
        0xe5, 0xfb, 0xc1, 0x19, 0x1a, 0x0a, 0x52, 0xef,
        0xf6, 0x9f, 0x24, 0x45, 0xdf, 0x4f, 0x9b, 0x17,
        0xad, 0x2b, 0x41, 0x7b, 0xe6, 0x6c, 0x37, 0x10};
	printf("\nInitial message (length 64 bytes):");
	for (i = 0; i < 64; i++)
	{
		if (i % 16 == 0) {
			printf("\n");
		} else if (i % 4 == 0) {
			printf(" ");
		}
		printf("%02x", test_string[i]);
	}
    printf("\n\nInitial key:\n");
    printAESblock(key);
    
	aesblock MIC;
    MIC = getMIC(test_string, key, 64);
	
	printf("\n");
    printf("Expected MIC:   bfbef051\n");
    printf("Calculated MIC: %02x%02x%02x%02x \n\n", MIC.data[3], MIC.data[2], MIC.data[1], MIC.data[0]);
    return 0;
}
