#ifndef AES_H
#define AES_H

#include "../datatype.h"

typedef struct
{
    uint8_t data[16];
} aesblock;

extern const uint8_t sbox[256];
extern const uint8_t Rcon[32];
extern const uint8_t AES_mx;
extern const uint8_t AES128_Nk;
extern const uint8_t AES128_Nr;
extern const uint8_t AESCMAC_Rb;

aesblock getMIC(void * src, aesblock key, size_t length);
aesblock Generate_Subkey_AESCMAC(aesblock key, unsigned char keynum);
void shift_block_left(aesblock& src);

aesblock computeAESblock(aesblock src, aesblock key);
void AES128oneround(aesblock& src, aesblock key);
void SubBytes(aesblock&src);
void ShiftRows(aesblock& src);
void MixColumns(aesblock& src);
void AddRoundKey(aesblock& src, aesblock key);

void KeyExpansion(aesblock init_key, aesblock* key_sched, size_t sched_len);
void RotWord(uint8_t* word);
void SubWord(uint8_t* word);

void Mix_one_Column(uint8_t& col_0, uint8_t& col_1, uint8_t& col_2, uint8_t& col_3);

uint8_t xtime(uint8_t inbyte, uint8_t power);

#endif // AES_H
