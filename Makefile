CC = g++
CC2 = gcc
CINCL = -I./AES -I./CRC -I./FEC -I./util -I.
CFLAGS = -Wall $(CINCL)

default: clean

test: test.out ;

test.out: aes.o crc.o fec.o eda.o aestest.o test.o edatest.o
	$(CC) aes.o crc.o fec.o eda.o aestest.o test.o edatest.o -o test.out $(CFLAGS)

test.o: test.cpp
	$(CC) -c test.cpp -o test.o $(CFLAGS)

test2: test2.out ;

test2.out: aes.o crc.o fec.o eda.o aestest.o test2.o edatest.o
	$(CC) aes.o crc.o fec.o eda.o aestest.o test2.o edatest.o -o test2.out $(CFLAGS)

test2.o: test.cpp
	$(CC) -c test2.cpp -o test2.o $(CFLAGS)

eda.o: eda.cpp
	$(CC) -c eda.cpp -o eda.o $(CFLAGS)

edatest.o: util/edatest.cpp
	$(CC) -c util/edatest.cpp -o edatest.o $(CFLAGS)

aes.o: AES/aes.cpp
	$(CC) -c AES/aes.cpp -o aes.o $(CFLAGS)
	
aestest.o: util/aestest.cpp
	$(CC) -c util/aestest.cpp -o aestest.o $(CFLAGS)
	
crc.o: CRC/crc.cpp
	$(CC) -c CRC/crc.cpp -o crc.o $(CFLAGS)
	
fec.o: FEC/fec.cpp
	$(CC) -c FEC/fec.cpp -o fec.o $(CFLAGS)

sandwich: clean

clean:
	- rm test.out
	- rm test2.out
	- rm eda.o
	- rm aes.o
	- rm crc.o
	- rm fec.o
	- rm aestest.o
	- rm edatest.o
	- rm test.o
	- rm test2.o
