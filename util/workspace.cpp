#include "workspace.h"

workspace_t StringToWorkspace(void* str, size_t strlen)
{
	uint8_t* test_data = (uint8_t*) str;
	workspace_t testspace;
	size_t i;
	if (strlen < 721)
	{
		testspace.info = 0x00;
		for (i = 0; i < 16; i++)
		{
			testspace.key[i] = 0x00;
		}
		testspace.length[0] = 0x00;
		testspace.length[1] = 0x00;
		for (i = 0; i < 700; i++)
		{
			testspace.data[i] = 0x00;
		}
		testspace.output[0] = 0x00;
		testspace.output[1] = 0x00;
	}
	else
	{
		testspace.info = test_data[0];
		for (i = 0; i < 16; i++)
		{
			testspace.key[i] = test_data[i + 1];
		}
		testspace.length[0] = test_data[17];
		testspace.length[1] = test_data[18];
		for (i = 0; i < 700; i++)
		{
			testspace.data[i] = test_data[19 + i];
		}
		testspace.output[0] = test_data[719];
		testspace.output[1] = test_data[720];
	}
	return testspace;
}
