#include "aestest.h"

void printAESblock(aesblock src)
{
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            printf("%02x  ", src.data[i + 4*j]);
        }
        printf("\n");
    }
    return;
}
