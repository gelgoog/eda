#include "edatest.h"
#include <stdio.h>

void PrintWorkspace(workspace_t ws, unsigned char initial_run)
{
	unsigned char decoded = (ws.output[1] & 0x80) >> 7;
	size_t i, init_msglen, msglen, sf;
	init_msglen = ((ws.length[1] & 0xff) << 8) | (ws.length[0] & 0xff);
	msglen = ((ws.output[1] & 0x07) << 8) | (ws.output[0] & 0xff);
	sf = ws.info & 0x0f;
	
	aesblock aeskey;
	for (i = 0; i < 16; i++)
	{
		aeskey.data[i] = ws.key[i];
	}
	if (initial_run)
	{
		printf("\nSF: %02d, Length: %03d\n", sf, init_msglen);
		printf("Data:\n");
		for (i = 0; i < init_msglen; i++)
		{
			if (i % 16 == 0)
			{
				printf("\n");
			}
			else if (i % 4 == 0)
			{
				printf(" ");
			}
			printf("%02x", ws.data[i]);
		}
		printf("\n");
		printf("\nAES key:\n");
		printAESblock(aeskey);
		printf("\n\n");
	}
	else
	{
		printf("\nLength: %03d, Decoded = %01d\n", msglen, decoded);
		printf("Data:");
		for (i = 0; i < msglen; i++)
		{
			if (i % 16 == 0)
			{
				printf("\n");
			}
			else if (i % 4 == 0)
			{
				printf(" ");
			}
			printf("%02x", ws.data[i]);
		}
		printf("\n");
	}
	return;
}

void PrintWorkspaceString(workspace_t ws)
{
	printf("\nWorkspace:\n");
	printf("%02x", ws.info);
	size_t i;
	for (i = 0; i < 16; i++)
	{
		printf("%02x", ws.key[i]);
	}
	printf("%02x%02x", ws.length[0], ws.length[1]);
	for (i = 0; i < 700; i++)
	{
		printf("%02x", ws.data[i]);
	}
	printf("%02x%02x\n", ws.output[0], ws.output[1]);
}
