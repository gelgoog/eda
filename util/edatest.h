#ifndef EDATEST_H
#define EDATEST_H

#include "eda.h"
#include "aestest.h"
#include <stdio.h>

void PrintWorkspace(workspace_t ws, unsigned char initial_run);
void PrintWorkspaceString(workspace_t ws);

#endif // EDATEST_H
