#ifndef WORKSPACE_H
#define WORKSPACE_H

#include "../eda.h"

workspace_t StringToWorkspace(void* str, size_t strlen);

#endif // WORKSPACE_H
