//#include<stdio.h>
#include "eda.h"

size_t SFtoPHDRlen(size_t sf)
{
	return (sf - 2);
}

size_t CalculateEncapsulatedSize(size_t MPsize, size_t PHDRlen)
{
    size_t newsize;
    newsize = MPsize;
    newsize += 1;   //MHDR
    newsize += 4;   //MIC
    newsize += 2;   //CRC
    newsize += PHDRlen;
    newsize += 2;   //PHDR_CRC
    
    newsize *= 2;   //Hamming (8,4)
    return newsize;
}


// data starts with PHDR | empty CRC | MHDR | MACPayload | Empty MIC | Empty CRC
int EncodeMIC (void* needsMIC)
{
	workspace_t* src = (workspace_t*) needsMIC;
	
	size_t sf = src->info & 0xf;
	size_t PHDRlen = SFtoPHDRlen(sf);
	size_t i;
	size_t msglen = ((src->length[1] <<8) & 0xff00) | (src->length[0] & 0x00ff);
	msglen -= (PHDRlen + 2 + 4 + 2); //PCRC, MIC, pCRC

	// Shift everything after the PHDR_CRC space by 16 bytes
	// so we have room for B_0
	for (i = (msglen + 4 + 2); i > 0; i--)
	{
		src->data[PHDRlen + 2 + 16 + i] = src->data[PHDRlen + 2 + i];
	}
	src->data[PHDRlen + 2 + 16] = src->data[PHDRlen + 2];
	

	// The first 16 bytes for MIC calculation should be B0.
	// The data we want is in FHDR, the first bytes of MACPayload.
	// MACPayload starts at byte 18, after PHDR + PCRC since byte 17 is MHDR.
	
	uint8_t Dir;
	if (src->info && 0x80) { //sent/received bit
    	Dir = 0x00; //0 for Uplink
	} else {
		Dir = 0x01;	//1 for Downlink
	}
	
    uint8_t DevAddr[4];
    uint8_t FCntUp[4];

    DevAddr[0] = src->data[PHDRlen + 2 + 17 + 0];
    DevAddr[1] = src->data[PHDRlen + 2 + 17 + 1];
    DevAddr[2] = src->data[PHDRlen + 2 + 17 + 2];
    DevAddr[3] = src->data[PHDRlen + 2 + 17 + 3];
    FCntUp[0] = 0x00;	//As of right now, we're using
    FCntUp[1] = 0x00;	// 16-bit FCnts.
    FCntUp[2] = src->data[PHDRlen + 2 + 17 + 5];
    FCntUp[3] = src->data[PHDRlen + 2 + 17 + 6];
    
    //B0.
	src->data[PHDRlen + 2 + 0] = 0x49;
    src->data[PHDRlen + 2 + 1] = 0x00;
    src->data[PHDRlen + 2 + 2] = 0x00;
    src->data[PHDRlen + 2 + 3] = 0x00;
    src->data[PHDRlen + 2 + 4] = 0x00;
    src->data[PHDRlen + 2 + 5] = Dir;
    src->data[PHDRlen + 2 + 6] = DevAddr[0];
    src->data[PHDRlen + 2 + 7] = DevAddr[1];
    src->data[PHDRlen + 2 + 8] = DevAddr[2];
    src->data[PHDRlen + 2 + 9] = DevAddr[3];
    src->data[PHDRlen + 2 + 10] = FCntUp[0];
    src->data[PHDRlen + 2 + 11] = FCntUp[1];
    src->data[PHDRlen + 2 + 12] = FCntUp[2];
    src->data[PHDRlen + 2 + 13] = FCntUp[3];
    src->data[PHDRlen + 2 + 14] = 0x00;
    src->data[PHDRlen + 2 + 15] = msglen & 0xff;
	
	aesblock aeskey;
	for (i = 0; i < 16; i++)
	{
		aeskey.data[i] = src->key[i];
	}
	
	aesblock MIC = getMIC(&(src->data[PHDRlen + 2]), aeskey, msglen + 16);
	
	for (i = 0; i < (msglen + 4 + 2); i++)
	{
		src->data[PHDRlen + 2 + i] = src->data[PHDRlen + 2 + 16 + i];
	}
	
	// Add the MIC
	src->data[PHDRlen + 2 + msglen + 0] = MIC.data[3];
	src->data[PHDRlen + 2 + msglen + 1] = MIC.data[2];
	src->data[PHDRlen + 2 + msglen + 2] = MIC.data[1];
	src->data[PHDRlen + 2 + msglen + 3] = MIC.data[0];
	// Just in case, zero out the pCRC
	src->data[PHDRlen + 2 + msglen + 4 + 0] = 0x00;
	src->data[PHDRlen + 2 + msglen + 4 + 1] = 0x00;
	
	// write the length into output
	msglen = PHDRlen + 2 + msglen + 4 + 2;
	src->output[1] = (msglen >> 8) & 0x07;
	src->output[0] = msglen & 0xff;
	
	return TASK_NO_ERR;
}

//data starts with PHDR | empty CRC | MHDR | MACPayload | MIC | Empty CRC
int EncodeCRC (void* needsCRC)
{
	workspace_t* src = (workspace_t*) needsCRC;

	//Gets PHDR and MHDR and stores them for safekeeping.	
	size_t sf = src->info & 0xf;
	size_t PHDRlen = SFtoPHDRlen(sf);

	size_t msglen = ((src->length[1] << 8) & 0xff00) | (src->length[0] & 0xff);
	msglen -= (PHDRlen + 2 + 2); //PCRC, pCRC

    uint16_t CRC, PHDR_CRC;
    CRC = getCRC(&(src->data[PHDRlen + 2]), msglen);
    PHDR_CRC = getCRC(src->data, PHDRlen);

	// Include PHDR_CRC.
	src->data[PHDRlen + 0] = (PHDR_CRC >> 8) & 0xff;
	src->data[PHDRlen + 1] = PHDR_CRC & 0xff;
	// Payload CRC.
	// Remember msglen is differently defined here as it was in EncodeMIC.
	src->data[PHDRlen + 2 + msglen + 0] = (CRC >> 8) & 0xff;
	src->data[PHDRlen + 2 + msglen + 1] = CRC & 0xff;

	return TASK_NO_ERR;
}

int EncodeFEC (void* NeedsFEC)
{
    workspace_t* src = (workspace_t*) NeedsFEC;
	size_t msglen = ((src->length[1] <<8) & 0xff00) | (src->length[0] & 0x00ff);
    FECEncode(0, src->data, msglen);
	msglen *= 2;
	src->output[1] = (msglen >> 8) & 0x07;
	src->output[0] = msglen & 0xff;

    return TASK_NO_ERR;
}

int DecodeFEC (void* HasFEC)
{
    workspace_t* src = (workspace_t*) HasFEC;
	size_t init_len;
	init_len = ((src->length[1] << 8) & 0xff00) | (src->length[0] & 0x00ff);
	size_t newlen = (init_len / 2) + (init_len % 2);

    bool success;
    success = FECDecode(0, src->data, init_len);
    if (!success) {
        src->output[1] |= DECODE_FAIL;
        return TASK_ERR;
    }
    
	src->output[1] = (newlen >> 8) & 0x07;
	src->output[0] = newlen & 0xff;
    return TASK_NO_ERR;
}

// What we should have:
// data[] = PHDR | PHDR_CRC | MHDR | MACPayload | MIC
// No Payload CRC for downlink.
int DecodeCRC (void* HasCRC)
{
    workspace_t* src = (workspace_t*) HasCRC;
	size_t PHDRlen;
	size_t sf = src->info & 0x0f;
	PHDRlen = SFtoPHDRlen(sf);
	uint16_t PHDR_CRC, test_CRC;
	PHDR_CRC = ((src->data[PHDRlen] << 8) & 0xff00);
	PHDR_CRC |= (src->data[PHDRlen +1] & 0x00ff);
	test_CRC = getCRC(src->data, PHDRlen) & 0xffff;

    if (PHDR_CRC != test_CRC) {
        src->output[1] |= DECODE_FAIL;
        return TASK_ERR;
    }
    
    return TASK_NO_ERR;
}

int DecodeMIC (void* HasMIC)
{
	workspace_t* src = (workspace_t*) HasMIC;
	size_t PHDRlen, msglen;
	size_t sf = src->info & 0xf;
	PHDRlen = SFtoPHDRlen(sf);
	msglen = ((src->output[1] << 8) & 0x0700) | (src->output[0] & 0x00ff);
	// msg in this case is MHDR | MACPayload, so cut it down
	msglen -= (PHDRlen + 2 + 4);	// PHDR_CRC and MIC

	size_t i;
	for (i = (msglen + 4 + 2); i > 0; i--)
	{
		src->data[PHDRlen + 2 + 16 + i] = src->data[PHDRlen + 2 + i];
	}
	src->data[PHDRlen + 2 + 16] = src->data[PHDRlen + 2];
	
	// The first 16 bytes should be B0.
	// The data we want is in FHDR, the first bytes of MACPayload.
	// MACPayload starts at byte 18, after PHDR and PCRC, since byte 17 is MHDR.
	
    uint8_t Dir;
	if (src->info && 0x80) { //sent/received bit
    	Dir = 0x00; //0 for Uplink
	} else {
		Dir = 0x01;	//1 for Downlink
	}
    uint8_t DevAddr[4];
    uint8_t FCntDown[4];

    DevAddr[0] = src->data[PHDRlen + 2 + 17 + 0];
    DevAddr[1] = src->data[PHDRlen + 2 + 17 + 1];
    DevAddr[2] = src->data[PHDRlen + 2 + 17 + 2];
    DevAddr[3] = src->data[PHDRlen + 2 + 17 + 3];
    FCntDown[0] = 0x00;	//As of right now, we're using
    FCntDown[1] = 0x00;	// 16-bit FCnts.
    FCntDown[2] = src->data[PHDRlen + 2 + 17 + 5];
    FCntDown[3] = src->data[PHDRlen + 2 + 17 + 6];
    
    //B0.
	src->data[PHDRlen + 2 + 0] = 0x49;
    src->data[PHDRlen + 2 + 1] = 0x00;
    src->data[PHDRlen + 2 + 2] = 0x00;
    src->data[PHDRlen + 2 + 3] = 0x00;
    src->data[PHDRlen + 2 + 4] = 0x00;
    src->data[PHDRlen + 2 + 5] = Dir;
    src->data[PHDRlen + 2 + 6] = DevAddr[0];
    src->data[PHDRlen + 2 + 7] = DevAddr[1];
    src->data[PHDRlen + 2 + 8] = DevAddr[2];
    src->data[PHDRlen + 2 + 9] = DevAddr[3];
    src->data[PHDRlen + 2 + 10] = FCntDown[0];
    src->data[PHDRlen + 2 + 11] = FCntDown[1];
    src->data[PHDRlen + 2 + 12] = FCntDown[2];
    src->data[PHDRlen + 2 + 13] = FCntDown[3];
    src->data[PHDRlen + 2 + 14] = 0x00;
    src->data[PHDRlen + 2 + 15] = msglen;
    
	/*
    uint32_t MIC, test_MIC;
    MIC = (src->data[PHDRlen + 2 + 16 + msglen + 0] << 24) & 0xff000000;
    MIC |= (src->data[PHDRlen + 2 + 16 + msglen + 1] << 16) & 0x00ff0000;
    MIC |= (src->data[PHDRlen + 2 + 16 + msglen + 2] << 8) & 0x0000ff00;
    MIC |= src->data[PHDRlen + 2 + 16 + msglen + 3] & 0x000000ff;
    */
	uint8_t MIC[4];
	aesblock test_MIC;
	MIC[0] = src->data[PHDRlen + 2 + 16 + msglen + 0];
	MIC[1] = src->data[PHDRlen + 2 + 16 + msglen + 1];
	MIC[2] = src->data[PHDRlen + 2 + 16 + msglen + 2];
	MIC[3] = src->data[PHDRlen + 2 + 16 + msglen + 3];
	
    aesblock aeskey;
	for (i = 0; i < 16; i++)
	{
		aeskey.data[i] = src->key[i];
	}
    test_MIC = getMIC(&(src->data[PHDRlen + 2]), aeskey, msglen + 16);

	// Put everything back.
	// If we really want to optimize, we'd
	// do the MIC check first.
	for (i = 0; i < (msglen + 4 + 2); i++)
	{
		src->data[PHDRlen + 2 + i] = src->data[PHDRlen + 2 + 16 + i];
	}

	// MIC check.
    if (test_MIC.data[3] != MIC[0]) {
        src->output[1] |= DECODE_FAIL;
        return TASK_ERR;
    } else if (test_MIC.data[2] != MIC[1]) {
        src->output[1] |= DECODE_FAIL;
        return TASK_ERR;
    } else if (test_MIC.data[1] != MIC[2]) {
        src->output[1] |= DECODE_FAIL;
        return TASK_ERR;
    } else if (test_MIC.data[0] != MIC[3]) {
        src->output[1] |= DECODE_FAIL;
        return TASK_ERR;
    }
    
    src->output[1] &= DECODE_SUCCESS;
    return TASK_NO_ERR;
}
